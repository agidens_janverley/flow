﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Agidens.Logging;

namespace Flow.IO.Resolvers
{
    public class AssemblyResolver : IAssemblyResolver
    {
        protected readonly ILoggerFactory LoggerFactory;
        protected readonly ILogger Logger;

        public AssemblyResolver(ILoggerFactory loggerFactory)
        {
            this.LoggerFactory = loggerFactory;
            Logger = this?.LoggerFactory?.GetLogger(this);
        }

        #region IAssemblyResolver

        public IEnumerable<Assembly> Resolve(string path)
        {
            Logger?.Log(LogLevels.Debug, $"Resolving assemblies for path [{path}]");
            var assemblies = new List<Assembly>();

            try
            {
                var dlls = Directory.GetFiles(path, "*.dll", SearchOption.TopDirectoryOnly);
                Logger?.Log(LogLevels.Debug, $"Resolved {dlls.Length} assemblies");
                
                foreach(var dll in dlls)
                {
                    Logger?.Log(LogLevels.Debug, $"Resolved assembly [{dll}]");
                    assemblies.Add(Assembly.LoadFile(dll));
                }
            }
            catch(Exception e)
            {
                Logger?.Log(LogLevels.Error, e.Message, e, path);
                throw;
            }

            return assemblies;
        }

        #endregion
    }
}