﻿using System.Collections.Generic;
using System.Linq;
using Agidens.Logging;
using Flow.Core;
using Flow.Core.Result;
using Ninject;

namespace Flow.Ninject
{
    public class BlockFactory : IBlockFactory
    {
        private readonly ILoggerFactory loggerFactory;
        private readonly ILogger logger;
        private readonly IKernel kernel;

        public BlockFactory(IKernel kernel, ILoggerFactory loggerFactory)
        {
            this.kernel = kernel;
            this.loggerFactory = loggerFactory;
            logger = this?.loggerFactory?.GetLogger(this);
        }

        public IEnumerable<string> GetAvailableBlockTypeNames()
        {
            var blockBindings = kernel.GetBindings(typeof(IBlock));

            return blockBindings?.Select(b => b.Metadata.Name);
        }

        public IResult<IBlock> CreateBlock(string name)
        {
            var result = CompositeResult<IBlock>.Ok();

            logger?.Log(LogLevels.Info, $"Creating block from name '{name}'");
            var block = kernel?.TryGet<IBlock>(name);

            if(block != null)
            {
                result.Value = block;
                logger?.Log(LogLevels.Info, $"Created block from name '{name}'");
            }
            else
            {
                result.AddIssue(IssueCodes.BlockFactory.BlockCreationUnsuccessful, new[]
                {
                    IssueParameters.BlockFactory.BlockType.WithValue(name)
                });
                logger?.Log(LogLevels.Info, $"Error creating block from name '{name}'.");
            }

            return result;
        }
    }
}