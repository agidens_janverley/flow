﻿namespace Flow.Blocks
{
    public enum WeighingTypes
    {
        Undefined = 0,
        EntryWeighing = 1,
        IntermediateWeighing = 2,
        ExitWeighing = 3,
        Free = 4
    }
}