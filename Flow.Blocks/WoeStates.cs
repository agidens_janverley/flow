﻿namespace Flow.Blocks
{
    public enum WoeStates
    {
        Open = 1,
        InProgress = 2,
        Closed = 3
    }
}