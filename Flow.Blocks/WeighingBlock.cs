﻿using System;
using Agidens.Logging;
using Flow.Core;
using Flow.Core.Attributes;
using Flow.Core.Result;

namespace Flow.Blocks
{
    public class WeighingBlock : FunctionalBlock
    {
        [Contribution]
        public WeighingDto Weighing { get; set; }

        public WeighingBlock(ILoggerFactory loggerFactory)
            : base("Weighing", null, loggerFactory)
        {
        }

        public override IResult<IBlock> Activate()
        {
            var result = base.Activate();
            Weighing = new WeighingDto
            {
                WeightInKg = 12625m,
                Timestamp = DateTime.Now,
                Weighbridge = "WB-" + Guid.NewGuid().ToString().Substring(0, 8),
                WeighingToken = "WT-" + Guid.NewGuid().ToString().Substring(0, 8)
            };

            return result;
        }
    }
}