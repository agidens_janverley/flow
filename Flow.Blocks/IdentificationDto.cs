﻿namespace Flow.Blocks
{
    public class IdentificationDto
    {
        public string Identifier { get; set; }
        public IdentificationTypes Type { get; set; }
    }
}