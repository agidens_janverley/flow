﻿namespace Flow.Blocks
{
    public class DriverDto : PersonDto
    {
        public string License { get; set; }
        
        public override string ToString()
        {
            return $"{base.ToString()} - { License }";
        }
    }
}