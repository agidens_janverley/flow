﻿using System.Linq;
using Agidens.Logging;
using Flow.Core;
using Flow.Core.Attributes;

namespace Flow.Blocks
{
    [Setting("X", "1")]
    public class HasValueReachedXBlock : BoolCondition
    {
        [Requirement]
        [Contribution]
        public int Value { get; set; }

        public int X => int.Parse(Settings.Single(s => s.Key == "X").Value);

        public HasValueReachedXBlock(ILoggerFactory loggerFactory)
            : base(nameof(HasValueReachedXBlock), null, loggerFactory)
        {
        }

        public override object Evaluate() => Value >= X;
    }
}