﻿using System;

namespace Flow.Blocks
{
    public class PersonDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }

        public override string ToString()
        {
            return $"{FirstName} {LastName} ({DateOfBirth:d})";
        }
    }
}