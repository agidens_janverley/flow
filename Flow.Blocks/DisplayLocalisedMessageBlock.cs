﻿using System;
using System.Linq;
using Agidens.Logging;
using Flow.Core;
using Flow.Core.Attributes;
using Flow.Core.Result;

namespace Flow.Blocks
{
    [Setting("ResourceKey", "Blah")]
    [Setting("Culture", "en")]
    public class DisplayLocalisedMessageBlock : FunctionalBlock
    {
        public string ResourceKey => Settings.Single(s => s.Key == nameof(ResourceKey)).Value;
        public string Culture => Settings.Single(s => s.Key == nameof(Culture)).Value;

        public DisplayLocalisedMessageBlock(ILoggerFactory loggerFactory)
            : base("Display Localised Message", null, loggerFactory)
        {
        }

        public override IResult<IBlock> Activate()
        {
            var result = base.Activate();
            logger.Log(LogLevels.Trace, $"ResourceKey [{ResourceKey}]; Culture [{Culture}]");

            return result;
        }
    }
}