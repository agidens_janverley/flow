﻿using Agidens.Logging;
using Flow.Core;
using Flow.Core.Attributes;
using Flow.Core.Result;

namespace Flow.Blocks
{
    public class StoreWeighingBlock : FunctionalBlock
    {
        [Requirement]
        public WeighingDto Weighing { get; set; }

        [Requirement]
        public WorkOrderDto WorkOrder { get; set; }

        [Contribution]
        public WeighingTypes WeighingType { get; set; }

        public StoreWeighingBlock(ILoggerFactory loggerFactory)
            : base("Store Weighing", null, loggerFactory)
        {
        }

        public override IResult<IBlock> Activate()
        {
            var result = base.Activate();
            WeighingType = WeighingTypes.EntryWeighing;

            return result;
        }
    }
}