﻿using System.Linq;
using Agidens.Logging;
using Flow.Core;
using Flow.Core.Attributes;
using Flow.Core.Result;

namespace Flow.Blocks
{
    [Setting("Increment", "1")]
    public class IncrementBlock : FunctionalBlock
    {
        public int Increment => int.Parse(Settings.Single(s => s.Key == nameof(Increment)).Value);

        [Requirement]
        [Contribution]
        public int Value { get; set; }

        public IncrementBlock(ILoggerFactory loggerFactory)
            : base("Increment", null, loggerFactory)
        {
        }

        public override IResult<IBlock> Activate()
        {
            var result = base.Activate();
            Value += Increment;

            return result;
        }
    }
}