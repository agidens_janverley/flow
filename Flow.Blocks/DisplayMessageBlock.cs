﻿using Agidens.Logging;
using Flow.Core;
using Flow.Core.Attributes;

namespace Flow.Blocks
{
    public class DisplayMessageBlock : FunctionalBlock
    {
        [Requirement]
        public string Message { get; set; }

        public DisplayMessageBlock(ILoggerFactory loggerFactory)
            : base("Display Message", null, loggerFactory)
        {
        }
    }
}