﻿using Agidens.Logging;
using Flow.Core;
using Flow.Core.Attributes;
using Flow.Core.Result;

namespace Flow.Blocks
{
    public class DetermineWeighingMessageBlock : FunctionalBlock
    {
        [Requirement]
        public WeighingTypes WeighingType { get; set; }

        [Contribution]
        public string Message { get; set; }

        public DetermineWeighingMessageBlock(ILoggerFactory loggerFactory)
            : base("Determine Weighing Message", null, loggerFactory)
        {
        }

        public override IResult<IBlock> Activate()
        {
            var result = base.Activate();
            Message = $"{WeighingType}: something blah blah...";

            return result;
        }
    }
}