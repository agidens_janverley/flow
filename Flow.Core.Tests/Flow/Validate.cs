﻿using System;
using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using Flow.Core.Result;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Flow
{
    [TestClass]
    public class Validate
    {
        private Core.Flow flow;

        [TestInitialize]
        public void Init()
        {
            flow = A.Fake<Core.Flow>();
            A.CallTo(() => flow.Validate()).CallsBaseMethod();

            A.CallTo(() => flow.Blocks).Returns(new List<BlockInstance>
            {
                CreateBlock(),
                CreateBlock(),
                CreateBlock()
            });
            A.CallTo(() => flow.Connections).Returns(new List<IConnection<Core.BlockKey>>
            {
                CreateBlockConnection(flow.Blocks[0].Key, flow.Blocks[1].Key),
                CreateBlockConnection(flow.Blocks[1].Key, flow.Blocks[2].Key)
            });
        }

        private BlockInstance CreateBlock()
        {
            var bk = A.Fake<Core.BlockKey>();
            A.CallTo(() => bk.Uuid).Returns(Guid.NewGuid());
            var block = A.Fake<Core.Block>();
            var flowBlock = A.Fake<BlockInstance>();
            A.CallTo(() => flowBlock.Block).Returns(block);
            A.CallTo(() => flowBlock.Key).Returns(bk);

            return flowBlock;
        }

        private IConnection<Core.BlockKey> CreateBlockConnection(Core.BlockKey from, Core.BlockKey to)
        {
            var bkc = A.Fake<BlockKeyConnection>();
            A.CallTo(() => bkc.From).Returns(from);
            A.CallTo(() => bkc.To).Returns(to);

            return bkc;
        }

        [TestMethod]
        public void WhenBlocksCollectionIsNotEmptyAndBlockKeysAreUnique_ACallToGetInitialBlockMustHaveHappened()
        {
            //Act
            var result = flow.Validate();

            //Assert
            A.CallTo(() => flow.GetInitialBlock()).MustHaveHappened(Repeated.Exactly.Once);
        }

        [TestMethod]
        public void WhenGetInitialBlockResultIsValid_ResultShouldBeValid()
        {
            //Arrange
            var getInitialBlockResult = A.Fake<IResult<BlockInstance>>();
            A.CallTo(() => getInitialBlockResult.IsOk).Returns(true);
            A.CallTo(() => flow.GetInitialBlock()).Returns(getInitialBlockResult);

            //Act
            var result = flow.Validate();

            //Assert
            A.CallTo(() => flow.GetInitialBlock()).MustHaveHappened(Repeated.Exactly.Once);
        }

        [TestMethod]
        public void WhenGetInitialBlockResultIsInvalid_ResultShouldBeInvalid()
        {
            //Arrange
            var getInitialBlockResult = A.Fake<IResult<BlockInstance>>();
            A.CallTo(() => getInitialBlockResult.IsOk).Returns(false);
            A.CallTo(() => flow.GetInitialBlock()).Returns(getInitialBlockResult);

            //Act
            var result = flow.Validate();

            //Assert
            result.IsOk.ShouldBeFalse();
        }

        [TestMethod]
        public void WhenBlocksCollectionIsEmpty_ResultShouldBeInvalidWithIssueCodeBlockCollectionEmpty()
        {
            //Arrange
            A.CallTo(() => flow.Blocks).Returns(new List<BlockInstance>());

            //Act
            var result = flow.Validate();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.Flow.BlockCollectionEmpty);
        }

        [TestMethod]
        public void When1OrMoreBlocksAreNull_ResultShouldBeInvalidWithIssueCodeArgumentNull()
        {
            //Arrange
            A.CallTo(() => flow.Blocks).Returns(new List<BlockInstance>
            {
                CreateBlock(),
                null,
                CreateBlock()
            });

            //Act
            var result = flow.Validate();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.General.ArgumentNull);
        }

        [TestMethod]
        public void When1OrMoreBlockKeysAreNull_ResultShouldBeInvalidWithIssueCodeArgumentNull()
        {
            //Arrange
            var block = CreateBlock();
            A.CallTo(() => block.Key).Returns(null);
            A.CallTo(() => flow.Blocks).Returns(new List<BlockInstance>
            {
                CreateBlock(),
                block,
                CreateBlock()
            });

            //Act
            var result = flow.Validate();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.General.ArgumentNull);
        }

        [TestMethod]
        public void When1OrMoreBlockInstanceBlocksAreNull_ResultShouldBeInvalidWithIssueCodeArgumentNull()
        {
            //Arrange
            var block = CreateBlock();
            A.CallTo(() => block.Block).Returns(null);
            A.CallTo(() => flow.Blocks).Returns(new List<BlockInstance>
            {
                CreateBlock(),
                block,
                CreateBlock()
            });

            //Act
            var result = flow.Validate();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.General.ArgumentNull);
        }

        [TestMethod]
        public void WhenNotAllBlocksHaveAUniqueKey_ResultShouldBeInvalidWithIssueCodeFlowBlockIdentifierNotUnique()
        {
            //Arrange
            var key1 = A.Fake<Core.BlockKey>();
            A.CallTo(() => key1.Uuid).Returns(Guid.NewGuid());
            var key3 = A.Fake<Core.BlockKey>();
            A.CallTo(() => key3.Uuid).Returns(Guid.NewGuid());
            var block1 = CreateBlock();
            A.CallTo(() => block1.Key).Returns(key1);
            var block2 = CreateBlock();
            A.CallTo(() => block2.Key).Returns(key1);
            var block3 = CreateBlock();
            A.CallTo(() => block3.Key).Returns(key3);
            A.CallTo(() => flow.Blocks).Returns(new List<BlockInstance>
            {
                block1,
                block2,
                block3
            });

            //Act
            var result = flow.Validate();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.Flow.BlockIdentifierNotUnique
                                             && i.Parameters.Any(p => p.Key == IssueParameters.Flow.Identifier.Key
                                                                      && (p.Value as Core.BlockKey) == key1));
        }

        [TestMethod]
        public void WhenAnyBlockHasRequirements_ACallToRequirementResolverIsRequirementMetMustHaveHappenedForEachRequirement()
        {
            //Arrange
            A.CallTo(() => flow.RequirementResolver.IsRequirementMet(A<IRequirement>.Ignored, A<IEnumerable<IEnumerable<IContribution>>>.Ignored)).Returns(Result<IRequirement>.Ok());

            var block1 = CreateBlock();
            A.CallTo(() => block1.Block.Requirements).Returns(new[]
            {
                A.Fake<IRequirement>(),
                A.Fake<IRequirement>(),
                A.Fake<IRequirement>()
            });
            var block2 = CreateBlock();
            A.CallTo(() => block2.Block.Requirements).Returns(new[]
            {
                A.Fake<IRequirement>(),
                A.Fake<IRequirement>()
            });
            var block3 = CreateBlock();
            A.CallTo(() => flow.Blocks).Returns(new List<BlockInstance>
            {
                block1,
                block2,
                block3
            });

            //Act
            flow.Validate();

            //Assert
            A.CallTo(() => flow.RequirementResolver.IsRequirementMet(block1.Block.Requirements.ElementAt(0), A<IEnumerable<IEnumerable<IContribution>>>.Ignored)).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => flow.RequirementResolver.IsRequirementMet(block1.Block.Requirements.ElementAt(1), A<IEnumerable<IEnumerable<IContribution>>>.Ignored)).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => flow.RequirementResolver.IsRequirementMet(block1.Block.Requirements.ElementAt(2), A<IEnumerable<IEnumerable<IContribution>>>.Ignored)).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => flow.RequirementResolver.IsRequirementMet(block2.Block.Requirements.ElementAt(0), A<IEnumerable<IEnumerable<IContribution>>>.Ignored)).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => flow.RequirementResolver.IsRequirementMet(block2.Block.Requirements.ElementAt(1), A<IEnumerable<IEnumerable<IContribution>>>.Ignored)).MustHaveHappened(Repeated.Exactly.Once);
        }
    }
}