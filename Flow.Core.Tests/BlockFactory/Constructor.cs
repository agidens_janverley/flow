﻿using System.Linq;
using Agidens.Logging;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.BlockFactory
{
    public class Constructor
    {
        [TestMethod]
        public void WhenCalled_PropertiesShouldMatchParameters()
        {
            //Arrange
            var type1 = typeof(int);
            var type2 = typeof(decimal);
            var type3 = typeof(float);
            var types = new[] { type1, type2, type3 };
            var loggerFactory = A.Fake<ILoggerFactory>();

            //Act
            var factory = new Core.BlockFactory(types, loggerFactory);

            //Assert
            factory.BlockTypes.Count().ShouldBe(types.Length);

            foreach(var type in types)
            {
                factory.BlockTypes.Contains(type).ShouldBeTrue();
            }
        }
    }
}