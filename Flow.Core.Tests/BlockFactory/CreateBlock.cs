﻿using System;
using System.Linq;
using FakeItEasy;
using Flow.Core.Result;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.BlockFactory
{
    [TestClass]
    public class CreateBlock
    {
        private Core.BlockFactory blockFactory;

        [TestInitialize]
        public void Init()
        {
            blockFactory = A.Fake<Core.BlockFactory>();
            A.CallTo(() => blockFactory.CreateBlock(A<string>.Ignored)).CallsBaseMethod();
        }

        [TestMethod]
        public void WhenBlockTypesCollectionDoesntContainRequestedType_InvalidResultWithIssueCodeBlockCreationUnsuccessfulShouldBeReturned()
        {
            //Arrange
            A.CallTo(() => blockFactory.BlockTypes).Returns(Enumerable.Empty<Type>());

            //Act
            var result = blockFactory.CreateBlock(Guid.NewGuid().ToString());

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.BlockFactory.BlockTypeDoesntExist);
        }
    }
}