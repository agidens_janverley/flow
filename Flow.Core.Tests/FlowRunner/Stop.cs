﻿using System;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;
using Flow.Core.Result;

namespace Flow.Core.Tests.FlowRunner
{
    [TestClass]
    public class Stop
    {
        private Core.FlowRunner flowRunner;
        private Core.IFlow flow;

        [TestInitialize]
        public void Init()
        {
            flow = A.Fake<Core.IFlow>();

            flowRunner = A.Fake<Core.FlowRunner>();
            A.CallTo(() => flowRunner.Stop()).CallsBaseMethod();
            A.CallTo(() => flowRunner.CanStop()).Returns(Result<bool>.Ok());
            A.CallTo(() => flowRunner.Flow).Returns(flow);
        }

        [TestMethod]
        public void WhenCalled_CanStopMustHaveBeenCalledOnce()
        {
            //Act
            flowRunner.Stop();

            //Assert
            A.CallTo(() => flowRunner.CanStop()).MustHaveHappened(Repeated.Exactly.Once);
        }

        [TestMethod]
        public void WhenCanStopReturnsTrueAndDeactivationOfActiveBlockInstanceSucceeds_ACallToSetActiveBlockMustHaveHappenedWithNullAsParameter()
        {
            //Arrange
            A.CallTo(() => flowRunner.CanStop()).Returns(Result<bool>.Ok());
            A.CallTo(() => flowRunner.ActiveBlockInstance.Block.Deactivate()).Returns(Result<IBlock>.Ok());

            //Act
            flowRunner.Stop();

            //Assert
            A.CallTo(() => flowRunner.SetActiveBlock(null)).MustHaveHappened(Repeated.Exactly.Once);
        }
        
        [TestMethod]
        public void WhenCanStopReturnsTrueAndDeactivationOfActiveBlockInstanceDoesNotSucceed_ACallToSetActiveBlockMustHaveHappenedWithNullAsParameter()
        {
            //Arrange
            A.CallTo(() => flowRunner.CanStop()).Returns(Result<bool>.Ok());
            A.CallTo(() => flowRunner.ActiveBlockInstance.Block.Deactivate()).Returns(Result<IBlock>.Ok().AddIssue(new IssueCode(Guid.NewGuid().ToString(), 0)));

            //Act
            flowRunner.Stop();

            //Assert
            A.CallTo(() => flowRunner.SetActiveBlock(null)).MustNotHaveHappened();
        }

        [TestMethod]
        public void WhenCanStopReturnsTrue_ACallToDeactivateMethodOnActiveBlockMustHaveHappened()
        {
            //Arrange
            A.CallTo(() => flowRunner.CanStop()).Returns(Result<bool>.Ok());
            var fb = A.Fake<BlockInstance>();
            A.CallTo(() => fb.Block).Returns(A.Fake<IBlock>());
            A.CallTo(() => flowRunner.ActiveBlockInstance).Returns(fb);

            //Act
            flowRunner.Stop();

            //Assert
            A.CallTo(() => flowRunner.ActiveBlockInstance.Block.Deactivate()).MustHaveHappened(Repeated.Exactly.Once);
        }

        [TestMethod]
        public void WhenCanStopReturnsFalse_ACallToDeactivateMethodOnActiveBlockMustNotHaveHappened()
        {
            //Arrange
            var invalidResult = A.Fake<Result<bool>>();
            A.CallTo(() => invalidResult.IsOk).Returns(false);
            A.CallTo(() => flowRunner.CanStop()).Returns(invalidResult);
            var fb = A.Fake<BlockInstance>();
            A.CallTo(() => fb.Block).Returns(A.Fake<IBlock>());
            A.CallTo(() => flowRunner.ActiveBlockInstance).Returns(fb);

            //Act
            flowRunner.Stop();

            //Assert
            A.CallTo(() => flowRunner.ActiveBlockInstance.Block.Deactivate()).MustNotHaveHappened();
        }

        [TestMethod]
        public void WhenCanStopReturnsFalse_ActiveBlockInstanceMustBeUnchanged()
        {
            //Arrange
            var invalidResult = A.Fake<Result<bool>>();
            A.CallTo(() => invalidResult.IsOk).Returns(false);
            A.CallTo(() => flowRunner.CanStop()).Returns(invalidResult);
            var fb = A.Fake<BlockInstance>();
            A.CallTo(() => fb.Block).Returns(A.Fake<IBlock>());
            A.CallTo(() => flowRunner.ActiveBlockInstance).Returns(fb);

            //Act
            flowRunner.Stop();

            //Assert
            flowRunner.ActiveBlockInstance.ShouldBe(fb);
        }

        [TestMethod]
        public void WhenResultOfCompleteActionIsSuccessFul_ResultValueShouldBeSetToFlow()
        {
            //Arrange
            A.CallTo(() => flowRunner.CanStop()).Returns(Result<bool>.Ok());
            A.CallTo(() => flowRunner.ActiveBlockInstance.Block.Deactivate()).Returns(Result<IBlock>.Ok());
            A.CallTo(() => flowRunner.SetActiveBlock(null)).Returns(Result<IFlow>.Ok());
            
            //Act
            var result = flowRunner.Stop();
            
            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(flowRunner.Flow);
        }
    }
}