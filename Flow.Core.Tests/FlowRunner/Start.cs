﻿using FakeItEasy;
using Flow.Core.Result;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.FlowRunner
{
    [TestClass]
    public class Start
    {
        private Core.FlowRunner flowRunner;
        private Core.IFlow flow;

        [TestInitialize]
        public void Init()
        {
            flow = A.Fake<Core.IFlow>();

            flowRunner = A.Fake<Core.FlowRunner>();
            A.CallTo(() => flowRunner.Start()).CallsBaseMethod();
            A.CallTo(() => flowRunner.Flow).Returns(flow);
        }

        [TestMethod]
        public void WhenCalled_CanStartMustHaveBeenCalledOnce()
        {
            //Act
            flowRunner.Start();

            //Assert
            A.CallTo(() => flowRunner.CanStart()).MustHaveHappened(Repeated.Exactly.Once);
        }

        [TestMethod]
        public void WhenCanStartReturnsTrue_ACallToSetActiveBlockMustHaveHappenedWithInitialBlockAsAParameter()
        {
            //Arrange
            A.CallTo(() => flowRunner.CanStart()).Returns(Result<bool>.Ok());
            A.CallTo(() => flow.GetInitialBlock()).Returns(Result<BlockInstance>.Ok(A.Fake<BlockInstance>()));

            //Act
            flowRunner.Start();

            //Assert
            A.CallTo(() => flowRunner.SetActiveBlock(flow.GetInitialBlock().Value)).MustHaveHappened(Repeated.Exactly.Once);
        }

        [TestMethod]
        public void WhenCanStartReturnsTrue_ACallToActivateMethodOnActiveBlockMustHaveHappened()
        {
            //Arrange
            A.CallTo(() => flowRunner.CanStart()).Returns(Result<bool>.Ok());
            var fb = A.Fake<BlockInstance>();
            A.CallTo(() => fb.Block).Returns(A.Fake<IBlock>());
            A.CallTo(() => flow.GetInitialBlock()).Returns(Result<BlockInstance>.Ok(fb));

            //Act
            flowRunner.Start();

            //Assert
            A.CallTo(() => flowRunner.ActiveBlockInstance.Block.Activate()).MustHaveHappened(Repeated.Exactly.Once);
        }
        
        [TestMethod]
        public void WhenResultOfCompleteActionIsSuccessFul_ResultValueShouldBeSetToFlow()
        {
            //Arrange
            A.CallTo(() => flowRunner.CanStart()).Returns(Result<bool>.Ok());
            A.CallTo(() => flowRunner.Flow.GetInitialBlock()).Returns(Result<BlockInstance>.Ok());
            A.CallTo(() => flowRunner.ClearHistory()).Returns(Core.Result.Result.Ok());
            A.CallTo(() => flowRunner.ActiveBlockInstance.Block.Activate()).Returns(Result<IBlock>.Ok());
            A.CallTo(() => flowRunner.SetActiveBlock(A<BlockInstance>.Ignored)).Returns(Result<IFlow>.Ok());
            
            //Act
            var result = flowRunner.Start();
            
            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(flowRunner.Flow);
        }
    }
}