﻿using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using Flow.Core.Result;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.FlowRunner
{
    [TestClass]
    public class CanContinue
    {
        private Core.FlowRunner flowRunner;
        private Core.IFlow flow;

        [TestInitialize]
        public void Init()
        {
            flow = A.Fake<Core.IFlow>();
            
            var bk = A.Fake<Core.BlockKey>();
            var fb1 = A.Fake<Core.BlockInstance>();
            A.CallTo(() => fb1.Key).Returns(bk);
            A.CallTo(() => fb1.Block).Returns(A.Fake<IBlock>());
            var fb2 = A.Fake<Core.BlockInstance>();
            A.CallTo(() => fb2.Block).Returns(A.Fake<IBlock>());

            var c1 = A.Fake<IConnection<Core.BlockKey>>();
            A.CallTo(() => c1.From).Returns(bk);
            A.CallTo(() => flow.Connections).Returns(new List<IConnection<Core.BlockKey>> { c1 });

            A.CallTo(() => flow.Blocks).Returns(new List<Core.BlockInstance> { fb1, fb2 });

            flowRunner = A.Fake<Core.FlowRunner>();
            A.CallTo(() => flowRunner.CanContinue()).CallsBaseMethod();
            A.CallTo(() => flowRunner.Flow).Returns(flow);
            A.CallTo(() => flowRunner.ActiveBlockInstance).Returns(fb1);
        }

        [TestMethod]
        public void WhenBlocksCollectionIsNotEmptyAndBlockPropertyOfActiveBlockIsNotNullAndConnectionsCollectionContainsConnectionFromActiveBlockToAnotherBlock_ResultShouldBeTrue()
        {
            //Act
            var result = flowRunner.CanContinue();

            //Assert
            result.IsOk.ShouldBeTrue();
        }
        
        [TestMethod]
        public void WhenFlowIsNull_ResultShouldBeFalse()
        {
            //Arrange
            A.CallTo(() => flowRunner.Flow).Returns(null);

            //Act
            var result = flowRunner.CanContinue();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.FlowRunner.FlowNotDefined);
        }

        [TestMethod]
        public void WhenBlocksCollectionIsNull_ResultShouldBeFalse()
        {
            //Arrange
            A.CallTo(() => flow.Blocks).Returns(null);

            //Act
            var result = flowRunner.CanContinue();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.Flow.BlockCollectionEmpty);
        }

        [TestMethod]
        public void WhenBlocksCollectionIsEmpty_ResultShouldBeFalse()
        {
            //Arrange
            A.CallTo(() => flow.Blocks).Returns(new List<BlockInstance>());

            //Act
            var result = flowRunner.CanContinue();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.Flow.BlockCollectionEmpty);
        }

        [TestMethod]
        public void WhenBlocksCollectionDoesNotContainActiveBlock_ResultShouldBeFalse()
        {
            //Arrange
            var fb = A.Fake<Core.BlockInstance>();
            A.CallTo(() => fb.Block).Returns(A.Fake<IBlock>());
            A.CallTo(() => flowRunner.ActiveBlockInstance).Returns(fb);

            //Act
            var result = flowRunner.CanContinue();

            //Assert
            result.IsOk.ShouldBeFalse();
            ///TODO: Check for specific issuecode
        }

        [TestMethod]
        public void WhenActiveBlockIsNull_ResultShouldBeFalse()
        {
            //Arrange
            A.CallTo(() => flowRunner.ActiveBlockInstance).Returns(null);

            //Act
            var result = flowRunner.CanContinue();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.FlowRunner.FlowNotActive);
        }

        [TestMethod]
        public void WhenBlockPropertyOfActiveBlockIsNull_ResultShouldBeFalse()
        {
            //Arrange
            A.CallTo(() => flowRunner.ActiveBlockInstance.Block).Returns(null);

            //Act
            var result = flowRunner.CanContinue();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.FlowRunner.FlowNotActive);
        }
        
        [TestMethod]
        public void WhenConnectionsCollectionOfFlowIsNull_ResultShouldBeFalse()
        {
            //Arrange
            A.CallTo(() => flow.Connections).Returns(null);

            //Act
            var result = flowRunner.CanContinue();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.Flow.FlowHasNoConnections);
        }
        
        [TestMethod]
        public void WhenConnectionsCollectionOfFlowIsEmpty_ResultShouldBeFalse()
        {
            //Arrange
            A.CallTo(() => flow.Connections).Returns(new List<IConnection<Core.BlockKey>>());

            //Act
            var result = flowRunner.CanContinue();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.Flow.FlowHasNoConnections);
        }

        [TestMethod]
        public void WhenConnectionsCollectionDoesNotContainConnectionFromActiveBlockToAnotherBlock_ResultShouldBeFalse()
        {
            //Arrange
            var bk = A.Fake<Core.BlockKey>();
            var c1 = A.Fake<IConnection<Core.BlockKey>>();
            A.CallTo(() => c1.From).Returns(bk);
            A.CallTo(() => flow.Connections).Returns(new List<IConnection<Core.BlockKey>> { c1 });

            //Act
            var result = flowRunner.CanContinue();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.FlowRunner.ActiveBlockHasNoOutgoingConnections);
        }
    }
}