﻿using Agidens.Logging;
using FakeItEasy;
using Flow.Core.Resolvers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.FlowRunner
{
    [TestClass]
    public class Constructor
    {
        [TestMethod]
        public void WhenCalled_PropertiesShouldMatchParameters()
        {
            //Arrange
            var blockResolver = A.Fake<IBlockResolver>();
            var requirementResolver = A.Fake<IRequirementResolver>();
            var loggerFactory = A.Fake<ILoggerFactory>();

            //Act
            var flowRunner = new Core.FlowRunner(blockResolver, requirementResolver, loggerFactory);

            //Assert
            flowRunner.BlockResolver.ShouldBe(blockResolver);
            flowRunner.RequirementResolver.ShouldBe(requirementResolver);
        }

        [TestMethod]
        public void WhenCalled_HistoryShouldBeEmpty()
        {
            //Act
            var flowRunner = new Core.FlowRunner(A.Fake<IBlockResolver>(), A.Fake<IRequirementResolver>(), A.Fake<ILoggerFactory>());

            //Assert
            flowRunner.History.ShouldBeEmpty();
        }

        [TestMethod]
        public void WhenCalled_ActiveBlockInstanceShouldBeNull()
        {
            //Act
            var flowRunner = new Core.FlowRunner(A.Fake<IBlockResolver>(), A.Fake<IRequirementResolver>(), A.Fake<ILoggerFactory>());

            //Assert
            flowRunner.ActiveBlockInstance.ShouldBeNull();
        }

        [TestMethod]
        public void WhenCalled_FlowShouldBeNull()
        {
            //Act
            var flowRunner = new Core.FlowRunner(A.Fake<IBlockResolver>(), A.Fake<IRequirementResolver>(), A.Fake<ILoggerFactory>());

            //Assert
            flowRunner.Flow.ShouldBeNull();
        }
    }
}