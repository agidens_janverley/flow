﻿using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.FlowRunner
{
    [TestClass]
    public class SetActiveBlock
    {
        private Core.FlowRunner flowRunner;
        private Core.IFlow flow;

        [TestInitialize]
        public void Init()
        {
            flow = A.Fake<Core.IFlow>();

            flowRunner = A.Fake<Core.FlowRunner>();
            A.CallTo(() => flowRunner.SetActiveBlock(A<BlockInstance>.Ignored)).CallsBaseMethod();
            A.CallTo(() => flowRunner.ActiveBlockInstance).CallsBaseMethod();
            A.CallTo(() => flowRunner.Flow).Returns(flow);
        }

        [TestMethod]
        public void WhenCalled_ActiveBlockInstanceShouldMatchParameter()
        {
            //Arrange
            var blockInstances = new[] { A.Fake<Core.BlockInstance>(), null };

            foreach(var blockInstance in blockInstances)
            {
                //Act
                flowRunner.SetActiveBlock(blockInstance);

                //Assert
                flowRunner.ActiveBlockInstance.ShouldBe(blockInstance);
            }
        }
    }
}