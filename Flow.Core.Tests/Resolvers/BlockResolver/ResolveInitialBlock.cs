﻿using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using Flow.Core.Resolvers;
using Flow.Core.Result;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Resolvers.BlockResolver
{
    [TestClass]
    public class ResolveInitialBlock
    {
        private IBlockResolver blockResolver;
        private List<IBlock> blocks;
        private List<IConnection<IBlock>> connections;

        [TestInitialize]
        public void Init()
        {
            blockResolver = A.Fake<Core.Resolvers.BlockResolver>();
            A.CallTo(() => blockResolver.ResolveInitialBlock(A<IEnumerable<IBlock>>.Ignored, A<IEnumerable<IConnection<IBlock>>>.Ignored)).CallsBaseMethod();

            blocks = new List<IBlock>
            {
                A.Fake<IBlock>(),
                A.Fake<IBlock>(),
                A.Fake<IBlock>()
            };

            connections = new List<IConnection<IBlock>>
            {
                CreateConnection(blocks[0], blocks[1]),
                CreateConnection(blocks[1], blocks[2]),
            };
        }

        [TestMethod]
        public void WhenOnlyASingleBlockIsProvided_ValidResultWithSingleBlockShouldBeReturned()
        {
            //Act
            var result = blockResolver.ResolveInitialBlock(blocks.Take(1), connections);

            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(blocks[0]);
        }

        [TestMethod]
        public void WhenOnlyASingleBlockIsProvidedAndConnectionsParameterIsNull_ValidResultWithSingleBlockShouldBeReturned()
        {
            //Act
            var result = blockResolver.ResolveInitialBlock(blocks.Take(1), null);

            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(blocks[0]);
        }

        [TestMethod]
        public void WhenOnlyASingleBlockIsProvidedAndConnectionsParameterIsEmptyCollection_ValidResultWithSingleBlockShouldBeReturned()
        {
            //Act
            var result = blockResolver.ResolveInitialBlock(blocks.Take(1), Enumerable.Empty<IConnection<IBlock>>());

            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(blocks[0]);
        }

        [TestMethod]
        public void WhenMultipleBlocksAndConnectionsExistAndAreValid_ValidResultWithFirstBlockWithoutIncomingConnectionsShouldBeReturned()
        {
            //Act
            var result = blockResolver.ResolveInitialBlock(blocks, connections);

            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(blocks[0]);
        }

        [TestMethod]
        public void WhenBlocksCollectionIsNull_InvalidResultWithIssueCodeBlockCollectionIsEmptyShouldBeReturned()
        {
            //Act
            var result = blockResolver.ResolveInitialBlock(null, connections);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.BlockResolver.BlockCollectionIsEmpty);
        }

        [TestMethod]
        public void WhenBlocksCollectionIsEmpty_InvalidResultWithIssueCodeBlockCollectionIsEmptyShouldBeReturned()
        {
            //Act
            var result = blockResolver.ResolveInitialBlock(Enumerable.Empty<IBlock>(), connections);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.BlockResolver.BlockCollectionIsEmpty);
        }

        [TestMethod]
        public void WhenMultipleBlocksWithoutIncomingConnectionsExist_InvalidResultWithIssueCodeInitialBlockCouldNotBeDeterminedShouldBeReturned()
        {
            //Arrange
            blocks.Add(A.Fake<IBlock>());

            //Act
            var result = blockResolver.ResolveInitialBlock(blocks, connections);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.BlockResolver.InitialBlockCouldNotBeDetermined);
        }

        #region Helpers

        private IConnection<IBlock> CreateConnection(IBlock from, IBlock to)
        {
            var connection = A.Fake<Core.BlockConnection>();
            A.CallTo(() => connection.From).Returns(from);
            A.CallTo(() => connection.To).Returns(to);

            return connection;
        }

        #endregion Helpers
    }
}