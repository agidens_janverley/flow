using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FakeItEasy;
using Flow.Core.Attributes;
using Flow.Core.Result;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Resolvers.MostRecentFirstRequirementResolver
{
    public class Test
    {
        public int TestPropertyWithoutAttribute { get; set; }
        [Requirement]
        public int TestPropertyWithAttribute { get; set; }
    }

    [TestClass]
    public class IsRequirementMet
    {
        private Test test = new Test();
        private Core.Resolvers.MostRecentFirstRequirementResolver resolver;
        private Core.Requirement requirement;
        private List<List<Core.IContribution>> contributions;

        public int TestPropertyWithoutAttribute { get; set; }
        [Requirement]
        public int TestPropertyWithAttribute { get; set; }

        [TestInitialize]
        public void Init()
        {
            resolver = A.Fake<Core.Resolvers.MostRecentFirstRequirementResolver>();
            A.CallTo(() => resolver.IsRequirementMet(A<Core.Requirement>.Ignored, A<IEnumerable<IEnumerable<Core.IContribution>>>.Ignored)).CallsBaseMethod();

            requirement = A.Fake<Core.Requirement>();
            A.CallTo(() => requirement.Property).Returns(GetType().GetProperty(nameof(TestPropertyWithAttribute)));

            contributions = new List<List<Core.IContribution>>
            {
                new List<Core.IContribution>
                {
                    CreateContribution(Guid.NewGuid().ToString()),
                    CreateContribution(),
                    CreateContribution()
                },
                null,
                new List<Core.IContribution>(),
                new List<Core.IContribution>
                {
                    CreateContribution(),
                    CreateContribution()
                }
            };
        }
        
        [TestMethod]
        public void WhenRequirementIsNull_ResultShouldBeInvalidWithIssueCodeArgumentNull()
        {
            //Arrange
            requirement = null;

            //Act
            var result = resolver.IsRequirementMet(requirement, contributions);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Value.ShouldBeNull();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.General.ArgumentNull);
        }

        [TestMethod]
        public void WhenRequirementPropertyIsNull_ResultShouldBeInvalidWithIssueCodeArgumentNull()
        {
            //Arrange
            A.CallTo(() => requirement.Property).Returns(null);

            //Act
            var result = resolver.IsRequirementMet(requirement, contributions);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Value.ShouldBeNull();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.General.ArgumentNull);
        }
        
        [TestMethod]
        public void WhenRequirementPropertyDoesNotContainRequirementAttribute_ResultShouldBeInvalidWithIssueCodeRequirementResolverRequirementAttributeNotFound()
        {
            //Arrange
            A.CallTo(() => requirement.Property).Returns(GetType().GetProperty(nameof(TestPropertyWithoutAttribute)));

            //Act
            var result = resolver.IsRequirementMet(requirement, contributions);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Value.ShouldBeNull();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.RequirementResolver.RequirementAttributeNotFound
                                             && i.Parameters.Any(p =>
                                                 p.Key == IssueParameters.RequirementResolver.PropertyName.Key
                                                 && (string) p.Value == nameof(test.TestPropertyWithoutAttribute)));
        }

        [TestMethod]
        public void WhenNoMatchingContributionIsFound_ResultShouldBeInvalidWithIssueCodeRequirementResolverNoMatchingContributionsFound()
        {
            //Act
            var result = resolver.IsRequirementMet(requirement, contributions);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Value.ShouldBeNull();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.RequirementResolver.NoMatchingContributionsFound);
        }

        [TestMethod]
        public void WhenContributionsCollectionIsNull_ResultShouldBeInvalidWithIssueCodeArgumentNull()
        {
            //Arrange
            contributions = null;

            //Act
            var result = resolver.IsRequirementMet(requirement, contributions);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Value.ShouldBeNull();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.General.ArgumentNull);
        }

        private Core.IContribution CreateContribution(string alias = null)
        {
            var contribution = A.Fake<Core.IContribution>();
            A.CallTo(() => contribution.Alias).Returns(alias);

            return contribution;
        }
    }
}