﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Setting
{
    [TestClass]
    public class Constructor
    {
        [TestMethod]
        public void WhenCalledNameAndValueShouldMatchParameters()
        {
            //Arrange
            var stringCollection = new[]
            {
                Guid.NewGuid().ToString(),
                null,
                string.Empty
            };

            foreach(var key in stringCollection)
            {
                foreach(var value in stringCollection)
                {
                    foreach(var defaultValue in stringCollection)
                    {
                        foreach(var description in stringCollection)
                        {
                            //Act
                            var setting = new Core.Setting(key, value, defaultValue, description);

                            //Assert
                            setting.Key.ShouldBe(key);
                            setting.Value.ShouldBe(value);
                            setting.DefaultValue.ShouldBe(defaultValue);
                            setting.Description.ShouldBe(description);
                        }
                    }
                }
            }
        }

        [TestMethod]
        public void WhenValueParameterIsNotProvided_ValueShouldBeNull()
        {
            //Act
            var setting = new Core.Setting(Guid.NewGuid().ToString(), defaultValue: Guid.NewGuid().ToString(), description: Guid.NewGuid().ToString());

            //Assert
            setting.Value.ShouldBeNull();
        }
        
        [TestMethod]
        public void WhenDefaultValueParameterIsNotProvided_DefaultValueShouldBeNull()
        {
            //Act
            var setting = new Core.Setting(Guid.NewGuid().ToString(), value: Guid.NewGuid().ToString(), description: Guid.NewGuid().ToString());

            //Assert
            setting.DefaultValue.ShouldBeNull();
        }
        
        [TestMethod]
        public void WhenDescriptionParameterIsNotProvided_DescriptionShouldBeNull()
        {
            //Act
            var setting = new Core.Setting(Guid.NewGuid().ToString(), value: Guid.NewGuid().ToString(), defaultValue: Guid.NewGuid().ToString());

            //Assert
            setting.Description.ShouldBeNull();
        }
    }
}