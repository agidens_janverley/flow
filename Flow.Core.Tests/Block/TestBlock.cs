using Flow.Core.Attributes;

namespace Flow.Core.Tests.Block
{
    [Setting(SettingValues.Setting1Name, SettingValues.Setting1DefaultValue, SettingValues.Setting1Description)]
    [Setting(SettingValues.Setting2Name, SettingValues.Setting2DefaultValue)]
    [Setting(SettingValues.Setting3Name)]
    internal class TestBlock : Core.Block
    {
        public static class SettingValues
        {
            public const string Setting1Name = "Setting1";
            public const string Setting1DefaultValue = "Setting1DefaultValue";
            public const string Setting1Description = "Setting1Description";
            
            public const string Setting2Name = "Setting2";
            public const string Setting2DefaultValue = "Setting2DefaultValue";
            
            public const string Setting3Name = "Setting3";
        }
        
        [Requirement]
        public string StreetName { get; set; }
        
        [Requirement]
        [Contribution]
        public int StreetNumber { get; set; }
        
        [Contribution]
        public string PostalCode { get; set; }
        
        public TestBlock()
            : base(null, null, null)
        {
        }
        
        public TestBlock(string name, string description)
            : base(name, description, null)
        {
        }
    }
}