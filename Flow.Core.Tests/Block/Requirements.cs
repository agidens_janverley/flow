using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Block
{
    [TestClass]
    public class Requirements
    {
        private TestBlock block;
        
        [TestInitialize]
        public void Init()
        {
            block = new TestBlock();
        }
        
        [TestMethod]
        public void WhenCalled_RequirementsCollectionShouldBeInitialised()
        {
            //Act
            var requirements = block.Requirements;

            //Assert
            Debug.WriteLine(block);
            requirements.Count.ShouldBe(2);
            requirements.ShouldContain(r => r.Property.Name == nameof(TestBlock.StreetName));
            requirements.ShouldContain(r => r.Property.Name == nameof(TestBlock.StreetNumber));
        }
        
        [TestMethod]
        public void WhenCalledAfterUpdatingARequirementValue_RequirementsCollectionShouldReflectUpdate()
        {
            //Arrange
            var requirements = block.Requirements;
            var newStreetName = Guid.NewGuid().ToString();
            var newStreetNumber = newStreetName.Length;
            
            //Assert
            Debug.WriteLine("Pre");
            Debug.WriteLine(block);
            requirements.ShouldContain(r => r.Property.Name == nameof(TestBlock.StreetName) && (string)r.Value == default(string));
            requirements.ShouldContain(r => r.Property.Name == nameof(TestBlock.StreetNumber) && (int)r.Value == default(int));
            
            //Act
            block.StreetName = newStreetName;
            block.StreetNumber = newStreetNumber;
            requirements = block.Requirements;

            //Assert
            Debug.WriteLine("Post");
            Debug.WriteLine(block);
            requirements.ShouldContain(r => r.Property.Name == nameof(TestBlock.StreetName) && (string)r.Value == newStreetName);
            requirements.ShouldContain(r => r.Property.Name == nameof(TestBlock.StreetNumber) && (int)r.Value == newStreetNumber);
        }
    }
}