using System;
using System.Diagnostics;
using System.Linq;
using Flow.Core.Tests.Resolvers.MostRecentFirstRequirementResolver;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Block
{
    [TestClass]
    public class SetSettingValue
    {
        private TestBlock block;
        
        [TestInitialize]
        public void Init()
        {
            block = new TestBlock();
        }

        [TestMethod]
        public void WhenCalled_ValueShouldMatchParameter()
        {
            //Arrange
            var newValue = Guid.NewGuid().ToString();

            //Assert
            Debug.WriteLine("Pre");
            Debug.WriteLine(block);
            var setting = block.Settings.Single(s => s.Key == TestBlock.SettingValues.Setting1Name);
            setting.Value.ShouldBe(TestBlock.SettingValues.Setting1DefaultValue);
            setting.DefaultValue.ShouldBe(TestBlock.SettingValues.Setting1DefaultValue);

            //Act
            block.SetSettingValue(TestBlock.SettingValues.Setting1Name, newValue);

            //Assert
            Debug.WriteLine("Post");
            Debug.WriteLine(block);
            setting = block.Settings.Single(s => s.Key == TestBlock.SettingValues.Setting1Name);
            setting.Value.ShouldBe(newValue);
            setting.DefaultValue.ShouldBe(TestBlock.SettingValues.Setting1DefaultValue);
        }
    }
    
    [TestClass]
    public class ResetSettingValue
    {
        private TestBlock block;
        
        [TestInitialize]
        public void Init()
        {
            block = new TestBlock();
        }

        [TestMethod]
        public void WhenCalled_ValueShouldMatchParameter()
        {
            //Arrange
            var newValue = Guid.NewGuid().ToString();
            block.SetSettingValue(TestBlock.SettingValues.Setting1Name, newValue);

            //Assert
            Debug.WriteLine("Pre");
            Debug.WriteLine(block);
            var setting = block.Settings.Single(s => s.Key == TestBlock.SettingValues.Setting1Name);
            setting.Value.ShouldBe(newValue);
            setting.DefaultValue.ShouldBe(TestBlock.SettingValues.Setting1DefaultValue);

            //Act
            block.ResetSettingValue(TestBlock.SettingValues.Setting1Name);

            //Assert
            Debug.WriteLine("Post");
            Debug.WriteLine(block);
            setting = block.Settings.Single(s => s.Key == TestBlock.SettingValues.Setting1Name);
            setting.Value.ShouldBe(TestBlock.SettingValues.Setting1DefaultValue);
            setting.DefaultValue.ShouldBe(TestBlock.SettingValues.Setting1DefaultValue);
        }
    }
}