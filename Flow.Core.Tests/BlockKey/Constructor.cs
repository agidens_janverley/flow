﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.BlockKey
{
    [TestClass]
    public class Constructor
    {
        [TestMethod]
        public void WhenCalled_UuidShouldMatchParameter()
        {
            //Arrange
            var guids = new[] { Guid.Empty, Guid.NewGuid() };

            //Act
            foreach(var guid in guids)
            {
                var key = new Core.BlockKey(guid);

                //Assert
                key.Uuid.ShouldBe(guid);
            }
        }
    }
}