﻿using System;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.BlockKey
{
    [TestClass]
    public class GetHashCode
    {
        private Core.BlockKey lhs;

        [TestInitialize]
        public void Init()
        {
            lhs = A.Fake<Core.BlockKey>();
            A.CallTo(() => lhs.GetHashCode()).CallsBaseMethod();
        }

        [TestMethod]
        public void WhenCalled_ResultShouldMatchTheHashCodeOfUuid()
        {
            //Arrange
            var guids = new[] { Guid.Empty, Guid.NewGuid() };

            foreach(var guid in guids)
            {
                A.CallTo(() => lhs.Uuid).Returns(guid);

                //Act
                var hcLhs = lhs.GetHashCode();
                var hcGuid = guid.GetHashCode();

                //Assert
                hcLhs.ShouldBe(hcGuid);
            }
        }
    }
}