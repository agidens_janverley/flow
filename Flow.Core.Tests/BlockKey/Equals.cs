﻿using System;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.BlockKey
{
    [TestClass]
    public class Equals
    {
        private Core.BlockKey lhs;

        [TestInitialize]
        public void Init()
        {
            lhs = A.Fake<Core.BlockKey>();
            A.CallTo(() => lhs.Equals(A<object>._)).CallsBaseMethod();
        }

        [TestMethod]
        public void WhenUuidOfBothKeysMatch_ResultShouldBeTrue()
        {
            //Arrange
            var uuid = Guid.NewGuid();
            A.CallTo(() => lhs.Uuid).Returns(uuid);
            var rhs = A.Fake<Core.BlockKey>();
            A.CallTo(() => rhs.Uuid).Returns(uuid);

            //Act
            var result = lhs.Equals(rhs);

            //Assert
            result.ShouldBeTrue();
        }

        [TestMethod]
        public void WhenParameterIsNotABlockKey_ResultShouldBeFalse()
        {
            //Arrange
            var uuid = Guid.NewGuid();
            A.CallTo(() => lhs.Uuid).Returns(uuid);
            var rhs = A.Fake<object>();

            //Act
            var result = lhs.Equals(rhs);

            //Assert
            result.ShouldBeFalse();
        }
    }
}