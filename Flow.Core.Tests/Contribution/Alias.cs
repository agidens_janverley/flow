using System;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Contribution
{
    [TestClass]
    public class Alias
    {
        private Core.Contribution contribution;
        
        [TestInitialize]
        public void Init()
        {
            contribution = A.Fake<Core.Contribution>();
            A.CallTo(() => contribution.Alias).CallsBaseMethod();
        }

        [TestMethod]
        public void WhenPropertyHasntBeenSet_ACallToPropertyNameMustaveHappened()
        {
            //Act
            var alias = contribution.Alias;
            
            //Assert
            A.CallTo(() => contribution.Property.Name).MustHaveHappened(Repeated.Exactly.Once);
        }
        
        [TestMethod]
        public void WhenPropertyHasBeenSet_ValueShouldMatch()
        {
            //Arrange
            contribution = new Core.Contribution();
            var newValue = Guid.NewGuid().ToString();
            contribution.Alias = newValue;
            
            //Act
            var alias = contribution.Alias;
            
            //Assert
            alias.ShouldBe(newValue);
        }
    }
}