﻿using System;
using System.Globalization;

namespace Flow.Wpf.Shared.Converters
{
    public class OffsetConverter : ValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(!(value is int intValue))
                return default(int);

            var intParameter = default(int);

            if(parameter is string strParameter)
            {
                double.TryParse(strParameter, out var dblParameter);
                intParameter = (int)dblParameter;
            }
            else if(parameter is int integerParameter)
                intParameter = integerParameter;

            return System.Convert.ChangeType(intValue + intParameter, targetType);
        }
    }
}