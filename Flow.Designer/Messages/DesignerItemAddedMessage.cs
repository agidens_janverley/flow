﻿namespace Flow.Designer.Messages
{
    public class DesignerItemAddedMessage<T>
    {
        public T Item { get; }

        public DesignerItemAddedMessage(T item)
        {
            Item = item;
        }
    }
}