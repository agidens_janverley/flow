﻿namespace Flow.Designer.Messages
{
    public class DesignerItemRemovedMessage<T>
    {
        public T Item { get; }

        public DesignerItemRemovedMessage(T item)
        {
            Item = item;
        }
    }
}