﻿using System;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Flow.Designer.ViewModels;
using Flow.Designer.ViewModels.Block;
using Flow.IO.Xml;
using Flow.IO.Xml.FlowArchive;

namespace Flow.Designer
{
    public class FlowArchiveParser : IFlowArchiveParser
    {
        private readonly IXmlReader xmlReader;

        public FlowArchiveParser(IXmlReader xmlReader)
        {
            this.xmlReader = xmlReader;
        }

        public FlowViewModel ToViewModel(IFlowArchive flowArchive)
        {
            var flowVm = new FlowViewModel(this, xmlReader);
            var flowLoadResult = xmlReader.Load(flowArchive.FlowFile);

            foreach(var block in flowLoadResult.Value.Blocks)
            {
                var blockVm = new BlockViewModel
                {
                    Uuid = block.Key.Uuid,
                    Name = block.Block.Name,
                    Type = block.Block.Type,
                    Location = new PointViewModel(GetLocation(block.Key.Uuid, flowArchive.DesignerFile))
                };

                foreach(var setting in block.Block.Settings)
                {
                    blockVm.Settings.Add(new SettingViewModel
                    {
                        Key = setting.Key,
                        Value = setting.Value
                    });
                }

                foreach(var requirement in block.Block.Requirements)
                {
                    blockVm.Requirements.Add(new RequirementViewModel
                    {
                        Property = requirement?.Property?.Name,
                        Value = requirement?.Value,
                        Type = requirement?.Property?.PropertyType
                    });
                }

                foreach(var contribution in block.Block.Contributions)
                {
                    blockVm.Contributions.Add(new ContributionViewModel
                    {
                        Property = contribution?.Property?.Name,
                        Value = contribution?.Value,
                        Type = contribution?.Property?.PropertyType
                    });
                }

                flowVm.Items.Add(blockVm);
            }

            return flowVm;
        }

        public IFlowArchive ToFlowArchive(FlowViewModel flowVm)
        {
            if(flowVm == null)
                return null;

            var flowFile = CreateFlowDocument(flowVm);
            var designerFile = CreateDesignerDocument(flowVm);

            return new FlowArchive(null, flowFile, designerFile);
        }

        public Point GetLocation(Guid uuid, XDocument xml)
        {
            var layoutElement = GetLayout(GetFlow(xml));
            var block = GetBlock(uuid, layoutElement);

            if(block == null)
                return new Point();

            var xAttr = block.Attribute(XmlSupportedNames.AttributeNames.X);
            var yAttr = block.Attribute(XmlSupportedNames.AttributeNames.Y);

            if(xAttr == null
               || yAttr == null)
                return new Point();

            if(!int.TryParse(xAttr.Value, out var x))
                return new Point();

            if(!int.TryParse(yAttr.Value, out var y))
                return new Point();

            return new Point(x, y);
        }

        private string GetType(Guid uuid, XDocument xml)
        {
            var blocksElement = GetFlow(xml).Descendants().SingleOrDefault(d => d.Name.LocalName == XmlSupportedNames.TagNames.Blocks);
            var block = GetBlock(uuid, blocksElement);

            if(block == null)
                throw new ArgumentException(nameof(uuid));

            var typeAttr = block.Attribute(XmlSupportedNames.AttributeNames.Type);

            if(typeAttr == null)
                throw new XmlException($"Block-element {uuid} does not contain {XmlSupportedNames.AttributeNames.Type}-attribute.");

            return typeAttr.Value;
        }

        private XElement GetFlow(XDocument xml) => xml?.Descendants().SingleOrDefault(p => p.Name.LocalName == XmlSupportedNames.TagNames.Flow);
        private XElement GetLayout(XElement element) => element?.Descendants().SingleOrDefault(p => p.Name.LocalName == XmlSupportedNames.TagNames.Layout);

        private XElement GetBlock(Guid uuid, XElement xmlElement)
        {
            var blocks = xmlElement?.Descendants(XmlSupportedNames.TagNames.Block);

            return blocks?.SingleOrDefault(b => b.HasAttributes
                                                && b.Attribute(XmlSupportedNames.AttributeNames.Uuid) != null
                                                && ParseAttributeValueAsGuid(b.Attribute(XmlSupportedNames.AttributeNames.Uuid)) == uuid);
        }

        private Guid ParseAttributeValueAsGuid(XAttribute attribute)
        {
            return Guid.TryParse(attribute?.Value, out var guid)
                ? guid
                : Guid.Empty;
        }

        private XDocument CreateFlowDocument(FlowViewModel flow)
        {
            var blocksElement = new XElement(XmlSupportedNames.TagNames.Blocks);

            foreach(var block in flow.Items.OfType<BlockViewModel>())
            {
                var blockElement = new XElement(XmlSupportedNames.TagNames.Block,
                    new XAttribute(XmlSupportedNames.AttributeNames.Uuid, block.Uuid),
                    new XAttribute(XmlSupportedNames.AttributeNames.Type, block.Type));

                var settingsElement = new XElement(XmlSupportedNames.TagNames.Settings);

                foreach(var setting in block.Settings)
                {
                    var settingElement = new XElement(XmlSupportedNames.TagNames.Setting,
                        new XAttribute(XmlSupportedNames.AttributeNames.Key, setting.Key),
                        new XAttribute(XmlSupportedNames.AttributeNames.Value, setting.Value ?? string.Empty));

                    settingsElement.Add(settingElement);
                }

                blockElement.Add(settingsElement);
                blocksElement.Add(blockElement);
            }

            var connectionsElement = new XElement(XmlSupportedNames.TagNames.BlockConnections);

            foreach(var connection in flow.Items.OfType<BlockConnectionViewModel>())
            {
                var connectionElement = new XElement(XmlSupportedNames.TagNames.BlockConnection,
                    new XAttribute(XmlSupportedNames.AttributeNames.From, connection.From.Uuid),
                    new XAttribute(XmlSupportedNames.AttributeNames.To, connection.To.Uuid));

                connectionsElement.Add(connectionElement);
            }

            var flowElement = new XElement(XmlSupportedNames.TagNames.Flow, blocksElement, connectionsElement);

            return new XDocument(new XDeclaration("1.0", "utf-8", "yes"), flowElement);
        }

        private XDocument CreateDesignerDocument(FlowViewModel flow)
        {
            var layoutElement = new XElement(XmlSupportedNames.TagNames.Layout);

            foreach(var block in flow.Items.OfType<BlockViewModel>())
            {
                var blockElement = new XElement(XmlSupportedNames.TagNames.Block,
                    new XAttribute(XmlSupportedNames.AttributeNames.Uuid, block.Uuid),
                    new XAttribute(XmlSupportedNames.AttributeNames.X, block.Location.X),
                    new XAttribute(XmlSupportedNames.AttributeNames.Y, block.Location.Y));

                layoutElement.Add(blockElement);
            }

            var flowElement = new XElement(XmlSupportedNames.TagNames.Flow, layoutElement);

            return new XDocument(new XDeclaration("1.0", "utf-8", "yes"), flowElement);
        }
    }
}