﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Flow.Core;
using Flow.Designer.Messages;
using Flow.IO.Xml;
using Flow.IO.Xml.FlowArchive;
using Microsoft.Win32;

namespace Flow.Designer.ViewModels
{
    public class DesignerViewModel : BaseViewModel
    {
        private readonly IFlowArchiveParser flowArchiveParser;
        private readonly IXmlReader xmlReader;
        private IFlowArchive flowArchive;

        public FlowViewModel Flow { get; set; }
        public AvailableBlocksViewModel AvailableBlocks { get; set; }

        public DesignerViewModel(IBlockFactory blockFactory, IFlowArchiveParser flowArchiveParser, IXmlReader xmlReader)
        {
            this.flowArchiveParser = flowArchiveParser;
            this.xmlReader = xmlReader;

            var blockTypes = blockFactory.GetAvailableBlockTypeNames().ToList();
            var instantiatedBlocks = blockTypes.Select(bt => new BlockViewModel(blockFactory.CreateBlock(bt)?.Value){Type = bt});
            AvailableBlocks = new AvailableBlocksViewModel();
            AvailableBlocks.Blocks.AddRange(instantiatedBlocks);
            Flow = new FlowViewModel(flowArchiveParser, xmlReader);
        }

        public void DesignerItemPreviewDragOver(DragEventArgs e)
        {
            e.Handled = true;
            //// See if this is a copy and the data includes an image.
            //if(!(e.Data.GetData(nameof(BlockViewModel)) is BlockViewModel blockInstance))
            //    return;

            //if((e.Effects & DragDropEffects.Copy) == 0)
            //    return;


            //// Save the current image.
            //OldImage = picDropTarget.Image;

            //    // Display the preview image.
            //    var bm = (Bitmap)e.Data.GetData(DataFormats.Bitmap, true);
            //    var copy_bm = (Bitmap)bm.Clone();
            //    using(var gr = Graphics.FromImage(copy_bm))
            //    {
            //        // Cover with translucent white.
            //        using(SolidBrush br =
            //            new SolidBrush(Color.FromArgb(128, 255, 255, 255)))
            //        {
            //            gr.FillRectangle(br, 0, 0, bm.Width, bm.Height);
            //        }
            //    }
            //    picDropTarget.Image = copy_bm;
        }

        public void DesignerItemDropped(IInputElement eventSource, DragEventArgs e)
        {
            if(!(e.Data.GetData(nameof(BlockViewModel)) is BlockViewModel block))
                return;

            if(((e.Effects & DragDropEffects.Copy) == 0)
               && ((e.Effects & DragDropEffects.Move) == 0))
                return;

            var p = e.GetPosition(eventSource ?? ((IInputElement)e.Source));

            if(e.Effects == DragDropEffects.Copy)
            {
                var droppedBlock = new BlockViewModel(block)
                {
                    Uuid = Guid.NewGuid(),
                    Location = new PointViewModel(p)
                };

                Flow.AddBlock(droppedBlock);
            }
            else if(e.Effects == DragDropEffects.Move)
            {
                var xPos = (int)(p.X - block.DragOffset.X);

                if(xPos < 0)
                    xPos = 0;

                var yPos = (int)(p.Y - block.DragOffset.Y);

                if(yPos < 0)
                    yPos = 0;

                block.Location.X = xPos;
                block.Location.Y = yPos;
            }

            e.Handled = true;
        }

        public void ClearSelection()
        {
            if(Flow?.SelectedItem == null)
                return;

            Flow.SelectedItem = null;
        }

        public void PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if(!(Flow?.SelectedItem is BlockViewModel selectedBlock))
                return;

            if(!Keyboard.IsKeyDown(Key.LeftCtrl)
               && !Keyboard.IsKeyDown(Key.RightCtrl))
                return;

            if(!(e.Source is FrameworkElement fe))
                return;

            if(!(fe.DataContext is BlockViewModel designerItem))
                return;

            var bcvm = new BlockConnectionViewModel
            {
                From = Flow.Items.OfType<BlockViewModel>().Single(b => b.Uuid == selectedBlock.Uuid),
                To = Flow.Items.OfType<BlockViewModel>().Single(b => b.Uuid == designerItem.Uuid)
            };
            EventAggregator.PublishOnUIThread(new DesignerItemAddedMessage<BlockConnectionViewModel>(bcvm));
        }

        public bool CanOpen() => true;

        public void Open()
        {
            var ofd = new OpenFileDialog
            {
                Multiselect = false,
                DefaultExt = ".flow",
                CheckFileExists = true,
                AddExtension = true,
                Filter = "Flow file|*.flow"
            };

            if(!(ofd.ShowDialog() ?? false))
                return;

            var filename = ofd.FileName;
            var result = new FlowArchiveReader().Load(filename);

            if(!result.IsOk)
                return;

            flowArchive = result.Value;
            Flow.Reset();
            var flowLoadResult = xmlReader.Load(flowArchive.FlowFile);

            if(flowLoadResult.IsOk)
            {
                foreach(var block in flowLoadResult.Value.Blocks)
                {
                    var key = block.Key.Uuid;
                    var location = flowArchiveParser.GetLocation(key, flowArchive.DesignerFile);
                    EventAggregator.PublishOnUIThread(new DesignerItemAddedMessage<BlockViewModel>(new BlockViewModel(block.Block, key, location)));
                }

                foreach(var connection in flowLoadResult.Value.Connections)
                {
                    var bcvm = new BlockConnectionViewModel
                    {
                        From = Flow.Items.OfType<BlockViewModel>().Single(b => b.Uuid == connection.From.Uuid),
                        To = Flow.Items.OfType<BlockViewModel>().Single(b => b.Uuid == connection.To.Uuid)
                    };
                    EventAggregator.PublishOnUIThread(new DesignerItemAddedMessage<BlockConnectionViewModel>(bcvm));
                }
            }
        }

        public bool CanSave() => Flow != null;

        public void Save()
        {
            var sfd = new SaveFileDialog
            {
                DefaultExt = ".flow",
                CheckPathExists = true,
                AddExtension = true,
                Filter = "Flow file|*.flow"
            };

            if(sfd.ShowDialog() ?? false)
            {
                var filename = sfd.FileName;
                var path = Path.GetDirectoryName(filename);
                var name = (filename.EndsWith(".flow", StringComparison.CurrentCultureIgnoreCase))
                    ? Path.GetFileNameWithoutExtension(filename)
                    : Path.GetFileName(filename);
                
                var fa = flowArchiveParser.ToFlowArchive(Flow);
                fa.Name = name;

                new FlowArchiveWriter().Save(fa, path);
            }
        }
    }
}