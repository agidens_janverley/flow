﻿namespace Flow.Designer.ViewModels
{
    public abstract class LocationBasedDesignerItemViewModel : DesignerItemViewModel, IHaveLocation
    {
        private PointViewModel location;

        public PointViewModel Location
        {
            get => location;
            set
            {
                if(Equals(value, location)) return;
                location = value;
                NotifyOfPropertyChange();
            }
        }
    }
}