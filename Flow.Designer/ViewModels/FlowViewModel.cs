﻿using System.Collections.Specialized;
using System.Linq;
using Caliburn.Micro;
using Flow.Core;
using Flow.Core.Resolvers;
using Flow.Designer.Messages;
using Flow.IO.Xml;

namespace Flow.Designer.ViewModels
{
    public class FlowViewModel
        : BaseViewModel
            , IHandle<DesignerItemAddedMessage<BlockViewModel>>
            , IHandle<DesignerItemRemovedMessage<BlockViewModel>>
            , IHandle<DesignerItemAddedMessage<BlockConnectionViewModel>>
            , IHandle<DesignerItemRemovedMessage<BlockConnectionViewModel>>
    {
        private object lockObject = new object();
        private BlockViewModel selectedBlock;
        private bool isValid;
        private DesignerItemViewModel selectedItem;
        private readonly IXmlReader xmlReader;
        private readonly IFlowArchiveParser flowArchiveParser;

        public bool IsOk
        {
            get => isValid;
            set
            {
                if(value == isValid)
                    return;

                isValid = value;
                NotifyOfPropertyChange();
            }
        }

        public BindableCollection<DesignerItemViewModel> Items { get; }

        public DesignerItemViewModel SelectedItem
        {
            get => selectedItem;
            set
            {
                if(Equals(value, selectedItem)) return;
                selectedItem = value;
                NotifyOfPropertyChange();
            }
        }

        public FlowViewModel(IFlowArchiveParser flowArchiveParser, IXmlReader xmlReader)
        {
            this.xmlReader = xmlReader;
            this.flowArchiveParser = flowArchiveParser;
            Items = new BindableCollection<DesignerItemViewModel>();
            Items.CollectionChanged += (sender, args) => IsOk = IsFlowValid();
            EventAggregator.Subscribe(this);
        }

        public void AddBlock(BlockViewModel block)
        {
            lock(lockObject)
            {
                Items?.Add(block);
            }
        }

        public void RemoveBlock(BlockViewModel block)
        {
            lock(lockObject)
            {
                Items?.Remove(block);
            }
        }

        public void AddBlockConnection(BlockConnectionViewModel blockConnection)
        {
            lock(lockObject)
            {
                Items?.Add(blockConnection);
            }
        }

        public void AddConnection(IHaveUniqueIdentifier from, IHaveUniqueIdentifier to)
        {
            var fromItem = Items?.OfType<BlockViewModel>()?.SingleOrDefault(i => i.Uuid == from.Uuid);
            var toItem = Items?.OfType<BlockViewModel>()?.SingleOrDefault(i => i.Uuid == to.Uuid);

            if(from == null
               || to == null)
                return;

            lock(lockObject)
            {
                Items?.Add(new BlockConnectionViewModel
                {
                    From = fromItem,
                    To = toItem
                });
            }
        }

        public void RemoveBlockConnection(BlockConnectionViewModel blockConnection)
        {
            lock(lockObject)
            {
                Items?.Remove(blockConnection);
            }
        }

        public void Reset()
        {
            SelectedItem = null;
            Items?.Clear();
        }

        #region Helpers

        private bool IsFlowValid()
        {
            var flowArchive = flowArchiveParser.ToFlowArchive(this);
            var flowLoadResult = xmlReader.Load(flowArchive?.FlowFile);

            return flowLoadResult.IsOk
                   && (flowLoadResult.Value.Validate()?.IsOk ?? false);
        }

        #endregion Helpers

        public void Handle(DesignerItemAddedMessage<BlockViewModel> message) =>  AddBlock(message.Item);

        public void Handle(DesignerItemRemovedMessage<BlockViewModel> message)
        {
            //Remove all connections from/to the blockInstance in question.
            var connections = Items.OfType<BlockConnectionViewModel>()
                .Where(c => c.From == message.Item || c.To == message.Item)
                .Distinct()
                .ToList();

            foreach(var c in connections)
                RemoveBlockConnection(c);

            //Remove the actual blockInstance.
            RemoveBlock(message.Item);
        }
        public void Handle(DesignerItemAddedMessage<BlockConnectionViewModel> message) => AddBlockConnection(message.Item);
        public void Handle(DesignerItemRemovedMessage<BlockConnectionViewModel> message) => RemoveBlockConnection(message.Item);
    }
}