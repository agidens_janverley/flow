﻿using System;

namespace Flow.Designer.ViewModels
{
    public interface IHaveUniqueIdentifier
    {
        Guid Uuid { get; set; }
    }
}