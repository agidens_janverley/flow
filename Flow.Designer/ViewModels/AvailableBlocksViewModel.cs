﻿using Caliburn.Micro;

namespace Flow.Designer.ViewModels
{
    public class AvailableBlocksViewModel : Screen
    {
        public BindableCollection<BlockViewModel> Blocks { get; }

        public AvailableBlocksViewModel()
        {
            Blocks = new BindableCollection<BlockViewModel>();
        }
    }
}