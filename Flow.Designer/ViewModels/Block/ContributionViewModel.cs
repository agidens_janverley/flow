﻿using System;
using Caliburn.Micro;
using Flow.Core;

namespace Flow.Designer.ViewModels.Block
{
    public class ContributionViewModel : Screen
    {
        private string property;
        private object value;
        private Type type;

        public string Property
        {
            get => property;
            set
            {
                if(value == property) return;
                property = value;
                NotifyOfPropertyChange();
            }
        }

        public object Value
        {
            get => value;
            set
            {
                if(Equals(value, this.value)) return;
                this.value = value;
                NotifyOfPropertyChange();
            }
        }

        public Type Type
        {
            get => type;
            set
            {
                if(Equals(value, type)) return;
                type = value;
                NotifyOfPropertyChange();
            }
        }

        public ContributionViewModel()
        {
        }

        public ContributionViewModel(IContribution contribution)
            : this()
        {
            if(contribution == null)
                return;

            Property = contribution.Property.Name;
            Value = contribution.Value;
            Type = contribution.Property.PropertyType;
        }

        public ContributionViewModel(ContributionViewModel contribution)
            : this()
        {
            Property = contribution.Property;
            Type = contribution.Type;
            Value = contribution.Value;
        }
    }
}