﻿using Caliburn.Micro;
using Flow.Core;

namespace Flow.Designer.ViewModels.Block
{
    public class SettingViewModel : Screen
    {
        private string key;
        private string value;

        public string Key
        {
            get => key;
            set
            {
                if(value == key) return;
                key = value;
                NotifyOfPropertyChange();
            }
        }

        public string Value
        {
            get => value;
            set
            {
                if(value == this.value) return;
                this.value = value;
                NotifyOfPropertyChange();
            }
        }

        public SettingViewModel()
        {
        }

        public SettingViewModel(ISetting setting)
            : this()
        {
            Key = setting.Key;
            Value = setting.Value;
        }

        public SettingViewModel(SettingViewModel setting)
            : this()
        {
            Key = setting.Key;
            Value = setting.Value;
        }
    }
}