﻿using System;
using System.Xml.Linq;
using Flow.Designer.ViewModels;
using Flow.IO.Xml.FlowArchive;

namespace Flow.Designer
{
    public interface IFlowArchiveParser
    {
        FlowViewModel ToViewModel(IFlowArchive flowArchive);
        IFlowArchive ToFlowArchive(FlowViewModel flowVm);
        Point GetLocation(Guid uuid, XDocument xml);
    }
}