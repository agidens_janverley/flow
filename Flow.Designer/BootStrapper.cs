﻿using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using Caliburn.Micro;
using Flow.Core;
using Flow.Core.Resolvers;
using Flow.Designer.ViewModels;
using Flow.IO.Resolvers;
using Flow.IO.Xml;
using Flow.IO.Xml.FlowArchive;
using Ninject;

namespace Flow.Designer
{
    public class BootStrapper : BootstrapperBase
    {
        private static readonly Agidens.Logging.ILogger Logger;
        private static readonly Agidens.Logging.ILoggerFactory loggerFactory;
        private static readonly IBlockFactory BlockFactory;
        private static readonly IBlockResolver BlockResolver;
        private static readonly IFlowArchiveReader FlowArchiveReader;
        private static readonly IXmlReader XmlReader;
        private static readonly IFlowArchiveParser FlowArchiveParser;
        private static readonly IBlocksResolver BlocksResolver;
        private static readonly IAssemblyResolver AssemblyResolver;

        static BootStrapper()
        {
            using(var kernel = new StandardKernel())
            {
                kernel.Load("NinjectConfig.xml");
                loggerFactory = kernel.Get<Agidens.Logging.ILoggerFactory>();
                Logger = loggerFactory?.GetLogger(typeof(BootStrapper).FullName);

                //Initialse assemblies
                AssemblyResolver = kernel.Get<IAssemblyResolver>();
                var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                var assemblies = AssemblyResolver.Resolve(path)?.ToList();

                //Resolve blocks
                BlocksResolver = kernel.Get<IBlocksResolver>();
                var models = BlocksResolver.Resolve(assemblies);
                var modelsArgument = new Ninject.Parameters.ConstructorArgument("types", models);
                BlockFactory = kernel.Get<IBlockFactory>(modelsArgument);

                //BlockFactory = kernel.Get<IBlockFactory>();
                BlockResolver = kernel.Get<IBlockResolver>();
                FlowArchiveReader = kernel.Get<IFlowArchiveReader>();
                XmlReader = kernel.Get<IXmlReader>();
                FlowArchiveParser = kernel.Get<IFlowArchiveParser>();
            }
        }

        public BootStrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            var vm = new DesignerViewModel(BlockFactory, FlowArchiveParser, XmlReader);
            IWindowManager windowManager;

            try
            {
                windowManager = IoC.Get<IWindowManager>();
            }
            catch
            {
                windowManager = new WindowManager();
            }

            windowManager.ShowWindow(vm);
        }
    }
}