﻿using System.Linq;
using Agidens.Logging;
using Agidens.Terminal.Suite.Service.OrderManager.Contract;
using Agidens.Terminal.Suite.Service.OrderManager.Dto;
using Agidens.Terminal.Suite.Shared.Enum;
using Flow.Core;
using Flow.Core.Attributes;

namespace Flow.Kiosk.Blocks.BadgeIdentification
{
    //[Setting("Image", description: "Name of an image that is located in the 'Resources'-folder. It shows the image if found, otherwise the regular icon is shown.")]
    [Setting("KeyboardConfigurationKey", description: "Name of the application setting that holds the keyboard configuration to show.")]
    //[Setting("CheckPersonAttached", "false", "Indication if a verification step needs to be performed to make sure the identification is attached to a person.")]
    //[Setting("CheckWorkOrderAttached", "false", "Indication if a verification step needs to be performed to make sure the identification is attached to a work order.")]
    [Setting("ConnectedAccessControlDevice", description: "Name of the access control device to use to verify if the user has access.")]
    public class BadgeIdentificationModel : FunctionalBlock
    {
        protected readonly IIdentificationManager identificationManager;

        public BadgeIdentificationModel(ILoggerFactory loggerFactory, IIdentificationManager identificationManager)
            : base("Badge Identification", "Identifies a badge either by it being scanned or by entering its identifier manually.", loggerFactory)
        {
            this.identificationManager = identificationManager;
        }

        //public string ImageUrl => Settings.Single(s => s.Key == "Image").Value;
        public string KeyboardConfigurationKey => Settings?.SingleOrDefault(s => s.Key == nameof(KeyboardConfigurationKey))?.Value;

        [Contribution]
        public IdentificationDto Identification { get; set; }

        public IdentificationDto SetIdentification(string identifier)
        {
            Identification = identificationManager.GetIdentificationByIdentifierAsync(IdentificationTypes.Badge, identifier).Result;

            return Identification;
        }
    }
}