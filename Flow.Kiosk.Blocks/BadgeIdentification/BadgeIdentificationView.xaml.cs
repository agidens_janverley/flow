﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Flow.Kiosk.Blocks.BadgeIdentification
{
    /// <summary>
    /// Interaction logic for BadgeIdentificationView.xaml
    /// </summary>
    public partial class BadgeIdentificationView : UserControl
    {
        public BadgeIdentificationView()
        {
            InitializeComponent();
        }
    }
}
