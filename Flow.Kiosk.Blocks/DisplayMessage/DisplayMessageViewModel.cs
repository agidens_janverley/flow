﻿using Agidens.Logging;
using Flow.Kiosk.Shared.Attributes;
using Flow.Kiosk.Shared.ViewModels;

namespace Flow.Kiosk.Blocks.DisplayMessage
{
    [Priority(0)]
    public class DisplayMessageViewModel : BaseViewModel<DisplayMessageModel>
    {
        public DisplayMessageViewModel(ILoggerFactory loggerFactory, DisplayMessageModel model)
            : base(loggerFactory)
        {
            SetModel(model);
            //ResourceKey = DateTime.Now.ToLongTimeString();
        }
    }
}