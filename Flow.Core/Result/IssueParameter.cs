﻿using System;
using System.Collections.Generic;

namespace Flow.Core.Result
{
    [Serializable]
    public struct IssueParameter
    {
        public string Key { get; }
        public object Value { get; private set; }

        public IssueParameter(string key, object value = null)
        {
            Key = key;
            Value = value;
        }

        public IssueParameter(KeyValuePair<string, object> keyValue)
            : this(keyValue.Key, keyValue.Value)
        {
        }

        public IssueParameter WithValue(object value)
        {
            Value = value;

            return this;
        }

        public override string ToString()
        {
            return $"{Key}: {Value}";
        }
    }
}