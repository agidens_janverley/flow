﻿//using System;

//namespace Flow.Core.Result
//{
//    public class ActionResult : IResult
//    {
//        public virtual string Message { get; set; }
//        public virtual bool IsOk => Exception == null;
//        public virtual Exception Exception { get; set; }

//        public static IResult Ok(string message = null) => new ActionResult
//        {
//            Message = message
//        };

//        public static IResult Nok(Exception ex, string message = null) => new ActionResult
//        {
//            Exception = ex,
//            Message = message ?? ex?.Message
//        };
//    }

//    public class ActionResult<T> : ActionResult, IResult<T>
//    {
//        public virtual T Result { get; set; }

//        public ActionResult()
//        {
//        }

//        public ActionResult(IResult result)
//        {
//            base.Message = result?.Message;
//            base.Exception = result?.Exception;
//        }

//        public static IResult<T> Ok(T result, string message = null) => new ActionResult<T>
//        {
//            Result = result,
//            Message = message
//        };

//        public static IResult<T> Nok(Exception ex, T result = default(T), string message = null) => new ActionResult<T>
//        {
//            Result = result,
//            Exception = ex,
//            Message = message ?? ex?.Message
//        };

//        public static IResult<T> Nok(IResult result) => new ActionResult<T>
//        {
//            Exception = result?.Exception,
//            Message = result?.Message ?? result?.Exception?.Message
//        };
//    }
//}