﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Flow.Core.Result
{
    [Serializable]
    public class Issue
    {
        public virtual IssueCode IssueCode { get; }
        public virtual IReadOnlyList<IssueParameter> Parameters { get; }
        public virtual Exception Exception { get; }

        public Issue(IssueCode issueCode, IEnumerable<IssueParameter> parameters = null, Exception exception = null)
        {
            IssueCode = issueCode;
            Parameters = new List<IssueParameter>(parameters ?? Enumerable.Empty<IssueParameter>());
            Exception = exception;
        }

        public override string ToString()
        {
            var parameterstring = Parameters.Any() ? $" ({string.Join(",", Parameters.Select(p => p.ToString()))})" : "";
            var exception = (Exception != null)
                ? $" Exception: {Exception}"
                : string.Empty;
            return $"{IssueCode}{parameterstring}{exception}";
        }
    }
}