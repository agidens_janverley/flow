﻿namespace Flow.Core.Result
{
    public struct IssueCode
    {
        public string Prefix { get; }
        public int Code { get; }

        public IssueCode(string prefix, int code)
        {
            Prefix = prefix;
            Code = code;
        }

        public static bool operator ==(IssueCode c1, IssueCode c2) => c1.Equals(c2);
        public static bool operator !=(IssueCode c1, IssueCode c2) => !c1.Equals(c2);

        public override int GetHashCode() =>  ToString().GetHashCode();

        public override bool Equals(object obj)
        {
            var result = false;

            if(obj is IssueCode issueCode)
            {
                result = ToString().Equals(issueCode.ToString());
            }

            return result;
        }

        public override string ToString() => $"{Prefix}-{Code}";
    }
}