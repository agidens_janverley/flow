﻿using System.Diagnostics.CodeAnalysis;

namespace Flow.Core.Result
{
    [ExcludeFromCodeCoverage]
    public static class IssueParameters
    {
        public static class General
        {
            public static readonly IssueParameter Exception = new IssueParameter(nameof(Exception));
            public static readonly IssueParameter Argument = new IssueParameter(nameof(Argument));
            public static readonly IssueParameter EmptyString = new IssueParameter(nameof(EmptyString));
        }

        public static class BlockFactory
        {
            public static readonly IssueParameter BlockType = new IssueParameter(nameof(BlockType));
        }

        public static class Flow
        {
            public static readonly IssueParameter Identifier = new IssueParameter(nameof(Identifier));
            public static readonly IssueParameter From = new IssueParameter(nameof(From));
            public static readonly IssueParameter To = new IssueParameter(nameof(To));
        }
        
        public static class FlowRunner
        {
            public static readonly IssueParameter ActiveBlock = new IssueParameter(nameof(ActiveBlock));
        }

        public static class FlowArchiveReader
        {
            public static readonly IssueParameter Path = new IssueParameter(nameof(Path));
            public static readonly IssueParameter ActualExtension = new IssueParameter(nameof(ActualExtension));
            public static readonly IssueParameter ExpectedExtension = new IssueParameter(nameof(ExpectedExtension));
            public static readonly IssueParameter Filename = new IssueParameter(nameof(Filename));
        }

        public static class RequirementResolver
        {
            public static readonly IssueParameter PropertyName = new IssueParameter(nameof(PropertyName));
        }

        public static class XmlReader
        {
            public static readonly IssueParameter File = new IssueParameter(nameof(File));
            public static readonly IssueParameter Path = new IssueParameter(nameof(Path));
            public static readonly IssueParameter ActualExtension = new IssueParameter(nameof(ActualExtension));
            public static readonly IssueParameter ExpectedExtension = new IssueParameter(nameof(ExpectedExtension));
            public static readonly IssueParameter Filename = new IssueParameter(nameof(Filename));
        }
    }
}