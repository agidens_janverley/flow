﻿using System;
using System.Collections.Generic;
using System.Linq;
using Agidens.Logging;
using Flow.Core.Result;

namespace Flow.Core
{
    public class BlockFactory : IBlockFactory
    {
        private readonly ILoggerFactory loggerFactory;
        private readonly ILogger logger;
        private readonly List<Type> blockTypes;

        public virtual IEnumerable<Type> BlockTypes => blockTypes;

        public BlockFactory(IEnumerable<Type> types, ILoggerFactory loggerFactory)
        {
            this.loggerFactory = loggerFactory;
            logger = loggerFactory?.GetLogger(this);
            blockTypes = types?.ToList();

            logger?.Log(LogLevels.Debug, $"Initialised {nameof(BlockFactory)} with types: {string.Join(", ", BlockTypes?.OrderBy(t => t.FullName)?.Select(t => t.FullName) ?? Enumerable.Empty<string>())}");
        }

        #region IBlockFactory

        public virtual IEnumerable<string> GetAvailableBlockTypeNames() => BlockTypes.Select(t => t.FullName);

        public virtual IResult<IBlock> CreateBlock(string name)
        {
            var result = CompositeResult<IBlock>.Ok();

            logger?.Log(LogLevels.Info, $"Creating block from name '{name}'");
            var blockType = BlockTypes.SingleOrDefault(t => t.FullName == name)
                            ?? BlockTypes.SingleOrDefault(t => t.Name == name);

            if(blockType == null)
            {
                result.AddIssue(IssueCodes.BlockFactory.BlockTypeDoesntExist, new[]
                {
                    IssueParameters.BlockFactory.BlockType.WithValue(name)
                });
                logger?.Log(LogLevels.Error, $"Block types collection doesn't contain type '{name}'");
            }
            else
            {
                logger?.Log(LogLevels.Info, $"Creating block from name '{name}'");

                try
                {
                    result.Value = (IBlock)Activator.CreateInstance(blockType, loggerFactory);
                    logger?.Log(LogLevels.Info, $"Created block from name '{name}'");
                }
                catch(Exception e)
                {
                    result.AddIssue(IssueCodes.BlockFactory.BlockCreationUnsuccessful, new[]
                    {
                        IssueParameters.BlockFactory.BlockType.WithValue(name),
                        IssueParameters.General.Exception.WithValue(e)
                    });
                    logger?.Log(LogLevels.Error, e, e.Message);
                }
            }

            return result;
        }

        #endregion IBlockFactory
    }
}