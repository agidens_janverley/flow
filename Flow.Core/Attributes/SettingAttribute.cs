﻿using System;

namespace Flow.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class SettingAttribute : Attribute
    {
        public virtual string Name { get; }
        public virtual string DefaultValue { get; }
        public virtual string Description { get; }

        public SettingAttribute(string name, string defaultValue = null, string description = null)
        {
            Name = name;
            DefaultValue = defaultValue;
            Description = description;
        }
    }
}