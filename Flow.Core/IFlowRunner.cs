﻿using Flow.Core.Result;

namespace Flow.Core
{
    public interface IFlowRunner
    {
        IFlow Flow { get; set; }
        BlockInstance ActiveBlockInstance { get; }

        IResult<bool> CanStart();
        IResult<IFlow> Start();
        IResult<bool> CanContinue();
        IResult<IFlow> Continue();
        IResult<bool> CanStop();
        IResult<IFlow> Stop();
    }
}