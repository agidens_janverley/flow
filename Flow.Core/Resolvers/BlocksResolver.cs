﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Flow.Core.Resolvers
{
    public class BlocksResolver : IBlocksResolver
    {
        public virtual IEnumerable<Type> Resolve(IEnumerable<Assembly> assemblies)
        {
            var result = new List<Type>();

            if(assemblies != null)
            {
                foreach(var assembly in assemblies)
                {
                    result.AddRange(assembly.GetExportedTypes().Where(t => typeof(IBlock).IsAssignableFrom(t) && !t.IsAbstract && (t.IsClass || t.IsValueType)));
                }
            }

            return result;
        }
    }
}