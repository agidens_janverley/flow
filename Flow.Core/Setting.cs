﻿namespace Flow.Core
{
    public class Setting : ISetting
    {
        public Setting(string key, string value = null, string defaultValue = null, string description = null)
        {
            Key = key;
            Value = value;
            DefaultValue = defaultValue;
            Description = description;
        }

        #region ISetting

        public virtual string Key { get; set; }
        public virtual string DefaultValue { get; set; }
        public virtual string Description { get; set; }
        public virtual string Value { get; set; }

        #endregion ISetting

        public override string ToString()
        {
            return $"{GetType().FullName} - Key [{Key}], Value [{Value}], DefaultValue [{DefaultValue}], Description [{Description}]";
        }
    }
}