﻿using System;
using System.Collections.Generic;
using System.Linq;
using Agidens.Logging;
using Flow.Core.Resolvers;
using Flow.Core.Result;

namespace Flow.Core
{
    public class FlowRunner : IFlowRunner
    {
        protected readonly ILogger logger;

        public virtual IFlow Flow { get; set; }
        public virtual BlockInstance ActiveBlockInstance { get; private set; }
        public virtual IBlockResolver BlockResolver { get; }
        public virtual IRequirementResolver RequirementResolver { get; }
        public virtual List<BlockInstance> History { get; }

        public FlowRunner(IBlockResolver blockResolver, IRequirementResolver requirementResolver, ILoggerFactory loggerFactory)
        {
            BlockResolver = blockResolver;
            RequirementResolver = requirementResolver;
            logger = loggerFactory?.GetLogger(this);
            History = new List<BlockInstance>();
        }

        public virtual IResult<bool> CanStart()
        {
            var result = CompositeResult<bool>.Ok();

            if(Flow == null)
            {
                result.AddIssue(IssueCodes.FlowRunner.FlowNotDefined);
            }
            else
            {
                if(Flow.Blocks == null
                   || !Flow.Blocks.Any())
                {
                    result.AddIssue(IssueCodes.Flow.BlockCollectionEmpty);
                }

                result.AddResult(Flow.GetInitialBlock());
            }

            if(ActiveBlockInstance != null)
            {
                result.AddIssue(IssueCodes.FlowRunner.FlowActive, new[]
                {
                    IssueParameters.FlowRunner.ActiveBlock.WithValue(ActiveBlockInstance)
                });
            }

            result.Value = result.IsOk;

            return result;
        }

        public virtual IResult<IFlow> Start()
        {
            logger.Log(LogLevels.Info, "Starting flow");
            
            var result = CompositeResult<IFlow>.Ok();
            result.AddResult(CanStart());
            
            if(result.IsOk)
            {
                var initialBlockResult = Flow.GetInitialBlock();
                result.AddResult(initialBlockResult);

                if(result.IsOk)
                {
                    result.AddResult(ClearHistory());
                    result.AddResult(SetActiveBlock(initialBlockResult.Value));
                    result.AddResult(ActiveBlockInstance.Block.Activate());
                }
            }

            if(result.IsOk)
            {
                result.Value = Flow;
                logger.Log(LogLevels.Info, "Flow started");
            }
            else
            {
                logger.Log(LogLevels.Error, "Error starting flow", result);
            }

            return result;
        }

        public virtual IResult<bool> CanContinue()
        {
            var result = CompositeResult<bool>.Ok();
            
            if(Flow == null)
            {
                result.AddIssue(IssueCodes.FlowRunner.FlowNotDefined);
            }
            else
            {
                if(Flow.Blocks == null
                   || !Flow.Blocks.Any())
                {
                    result.AddIssue(IssueCodes.Flow.BlockCollectionEmpty);
                }
            }
            
            if(ActiveBlockInstance?.Block == null)
            {
                result.AddIssue(IssueCodes.FlowRunner.FlowNotActive);
            }
            else
            {
                if(Flow != null)
                {
                    if(Flow.Connections == null
                       || (Flow.Connections != null
                           && !Flow.Connections.Any()))
                    {
                        result.AddIssue(IssueCodes.Flow.FlowHasNoConnections);
                    }
                    else
                    {
                        if(Flow.Connections.All(c => c.From != ActiveBlockInstance.Key))
                        {
                            result.AddIssue(IssueCodes.FlowRunner.ActiveBlockHasNoOutgoingConnections);
                        }
                    }
                }
            }
            
            result.Value = result.IsOk;

            return result;
        }

        public virtual IResult<IFlow> Continue()
        {
            logger.Log(LogLevels.Info, "Advancing flow");
            
            var result = CompositeResult<IFlow>.Ok();
            result.AddResult(CanContinue());

            if(result.IsOk)
            {
                result.AddResult(ActiveBlockInstance.Block.Deactivate());
                History.Add(new BlockInstance
                {
                    Key = ActiveBlockInstance.Key,
                    Block = (IBlock)ActiveBlockInstance.Block.Clone()
                });

                var nextBlockResult = BlockResolver.ResolveNextBlock(ActiveBlockInstance?.Block, Flow.Connections.Select(c => new BlockConnection(Flow.Blocks.SingleOrDefault(bb => bb.Key == c.From)?.Block, Flow.Blocks.SingleOrDefault(bb => bb.Key == c.To)?.Block, c.ContinuationValue)));
                result.AddResult(nextBlockResult);

                if(nextBlockResult.IsOk)
                {
                    var nextFlowBlock = Flow.Blocks.SingleOrDefault(b => b.Block == nextBlockResult.Value);
                    result.AddResult(SetActiveBlock(nextFlowBlock));
                    SatisfyRequirements(ActiveBlockInstance);
                    result.AddResult(ActiveBlockInstance?.Block.Activate());
                }
            }

            if(result.IsOk)
            {
                result.Value = Flow;
                logger.Log(LogLevels.Info, "Flow advanced");
            }
            else
            {
                logger.Log(LogLevels.Error, "Error advancing flow", result);
            }

            return result;
        }

        public virtual IResult<bool> CanStop()
        {
            var result = CompositeResult<bool>.Ok();
            
            if(Flow == null)
            {
                result.AddIssue(IssueCodes.FlowRunner.FlowNotDefined);
            }
            else
            {
                if(Flow.Blocks == null
                   || !Flow.Blocks.Any())
                {
                    result.AddIssue(IssueCodes.Flow.BlockCollectionEmpty);
                }
            }
            
            if(ActiveBlockInstance?.Block == null)
            {
                result.AddIssue(IssueCodes.FlowRunner.FlowNotActive);
            }
            
            result.Value = result.IsOk;

            return result;
        }

        public virtual IResult<IFlow> Stop()
        {
            logger.Log(LogLevels.Info, "Stopping flow");
            
            var result = CompositeResult<IFlow>.Ok();
            result.AddResult(CanStop());
            
            if(result.IsOk)
            {
                result.AddResult(ActiveBlockInstance.Block.Deactivate());

                if(result.IsOk)
                {
                    result.AddResult(SetActiveBlock(null));
                }
            }

            if(result.IsOk)
            {
                result.Value = Flow;
                logger.Log(LogLevels.Info, "Flow stopped");
            }
            else
            {
                logger.Log(LogLevels.Error, "Flow could not be stopped", result);
            }

            return result;
        }

        internal virtual IResult<IFlow> SetActiveBlock(BlockInstance blockInstance)
        {
            ActiveBlockInstance = blockInstance;

            return Result<IFlow>.Ok(Flow);
        }

        internal virtual IResult<IFlow> SatisfyRequirements(BlockInstance blockInstance)
        {
            var result = CompositeResult<IFlow>.Ok();

            if(blockInstance?.Block == null)
            {
                result.AddIssue(IssueCodes.General.ArgumentNull, new[]
                {
                    IssueParameters.General.Argument.WithValue(nameof(blockInstance))
                });
            }

            if(result.IsOk)
            {
                if(blockInstance.Block.Requirements?.Any() ?? false)
                {
                    var accumulatedContributions = GetAccumulatedContributions();
                    
                    foreach(var requirement in blockInstance.Block.Requirements)
                    {
                        result.AddResult(RequirementResolver.SatisfyRequirement(requirement, blockInstance.Block, accumulatedContributions));
                    }
                }
            }

            if(result.IsOk)
            {
                result.Value = Flow;
            }

            return result;
        }

        /// <summary>
        /// Gets all contributions made from blocks since the start of the flow run.
        /// </summary>
        /// <returns></returns>
        internal virtual List<IEnumerable<IContribution>> GetAccumulatedContributions() => GetAllContributions(History);

        /// <summary>
        /// Clears all historic info with regards to the current run.
        /// </summary>
        /// <returns></returns>
        internal virtual IResult ClearHistory()
        {
            var result = CompositeResult.Ok();

            if(ActiveBlockInstance != null)
            {
                result.AddIssue(IssueCodes.FlowRunner.FlowActive, new[]
                {
                    IssueParameters.FlowRunner.ActiveBlock.WithValue(ActiveBlockInstance)
                });
            }

            if(result.IsOk)
            {
                History.Clear();
            }

            return result;
        }

        /// <summary>
        /// Gets all contributions for the given collection of blocks.
        /// </summary>
        /// <param name="blocks"></param>
        /// <returns></returns>
        private List<IEnumerable<IContribution>> GetAllContributions(IEnumerable<BlockInstance> blocks)
        {
            var result = new List<IEnumerable<IContribution>>();

            if(blocks == null)
                return result;

            result.AddRange(blocks.Select(block => block.Block.Contributions));

            return result;
        }
    }
}