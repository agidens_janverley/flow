﻿using System;
using System.Collections.Generic;
using System.Linq;
using Agidens.Logging;
using Flow.Core.Resolvers;
using Flow.Core.Result;

namespace Flow.Core
{
    public class Flow : IFlow
    {
        private readonly ILogger logger;
        public virtual IBlockResolver BlockResolver { get; }
        public virtual IRequirementResolver RequirementResolver { get; }
        public virtual List<BlockInstance> Blocks { get; }
        public virtual List<IConnection<BlockKey>> Connections { get; }

        public Flow(IBlockResolver blockResolver, IRequirementResolver requirementResolver, ILoggerFactory loggerFactory)
        {
            BlockResolver = blockResolver;
            RequirementResolver = requirementResolver;
            logger = loggerFactory?.GetLogger(this);
            Blocks = new List<BlockInstance>();
            Connections = new List<IConnection<BlockKey>>();
        }

        #region IFlow

        public virtual IResult<BlockInstance> GetInitialBlock()
        {
            var result = CompositeResult<BlockInstance>.Ok();

            var ibResult = BlockResolver.ResolveInitialBlock(Blocks.Select(b => b.Block), Connections?.Select(c => new BlockConnection(Blocks.SingleOrDefault(bb => bb.Key == c.From)?.Block, Blocks.SingleOrDefault(bb => bb.Key == c.To)?.Block)));
            result.AddResult(ibResult);

            if(result.IsOk)
            {
                var flowBlock = Blocks.SingleOrDefault(b => b.Block == ibResult.Value);

                if(flowBlock == null)
                {
                    result.AddIssue(IssueCodes.Flow.BlockNotFoundInCollection);
                }
                else
                {
                    result.Value = flowBlock;
                }
            }

            return result;
        }

        public virtual IResult<IFlow> Validate()
        {
            var result = CompositeResult<IFlow>.Ok();

            if(!Blocks.Any())
            {
                result.AddIssue(IssueCodes.Flow.BlockCollectionEmpty);
            }

            if(result.IsOk)
            {
                if(Blocks.Any(b => b == null))
                {
                    result.AddIssue(IssueCodes.General.ArgumentNull, new[]
                    {
                        IssueParameters.General.Argument.WithValue(nameof(BlockInstance))
                    });
                }

                if(result.IsOk)
                {
                    if(Blocks.Any(b => b.Key == null))
                    {
                        result.AddIssue(IssueCodes.General.ArgumentNull, new[]
                        {
                            IssueParameters.General.Argument.WithValue(nameof(BlockInstance.Key))
                        });
                    }

                    if(Blocks.Any(b => b.Block == null))
                    {
                        result.AddIssue(IssueCodes.General.ArgumentNull, new[]
                        {
                            IssueParameters.General.Argument.WithValue(nameof(Block))
                        });
                    }

                    var accumulatedContributions = Blocks.Select(b => b?.Block?.Contributions ?? Enumerable.Empty<IContribution>()).ToList();

                    foreach(var flowBlock in Blocks)
                    {
                        foreach(var requirement in flowBlock?.Block?.Requirements ?? Enumerable.Empty<IRequirement>())
                        {
                            result.AddResult(RequirementResolver.IsRequirementMet(requirement, accumulatedContributions));
                        }
                    }

                    if(result.IsOk)
                    {
                        //Not all keys are unique.
                        if(Blocks.Count != Blocks.Select(b => b.Key.Uuid).Distinct().Count())
                        {
                            var nonUniqueIdentifiers = Blocks
                                .GroupBy(k => k.Key.Uuid)
                                .Where(g => g.Count() > 1)
                                .ToList();

                            nonUniqueIdentifiers.ForEach(i => result.AddIssue(IssueCodes.Flow.BlockIdentifierNotUnique, new[]
                            {
                                IssueParameters.Flow.Identifier.WithValue(new BlockKey(i.Key))
                            }));
                        }

                        if(result.IsOk)
                        {
                            var initialBlockResult = GetInitialBlock();
                            result.AddResult(initialBlockResult);
                        }
                    }
                }
            }

            return result;
        }

        public virtual IResult<IFlow> AddBlock(IBlock block) => AddBlock(new BlockKey(Guid.NewGuid()), block);

        public virtual IResult<IFlow> AddBlock(BlockKey key, IBlock block)
        {
            var result = CompositeResult<IFlow>.Ok();

            if(block == null)
            {
                result.AddIssue(IssueCodes.General.ArgumentNull, new[]
                {
                    IssueParameters.General.Argument.WithValue(nameof(block))
                });
            }

            if(key == null)
            {
                result.AddIssue(IssueCodes.General.ArgumentNull, new[]
                {
                    IssueParameters.General.Argument.WithValue(nameof(key))
                });
            }

            if(Blocks.Any(b => b.Key == key))
            {
                result.AddIssue(IssueCodes.Flow.BlockIdentifierNotUnique, new[]
                {
                    IssueParameters.Flow.Identifier.WithValue(key)
                });
            }

            if(result.IsOk)
            {
                var fb = new BlockInstance
                {
                    Key = key,
                    Block = block
                };
                Blocks.Add(fb);
                result.Value = this;
            }

            return result;
        }

        public virtual IResult<IFlow> RemoveBlock(BlockKey key)
        {
            var result = CompositeResult<IFlow>.Ok();

            Blocks.RemoveAll(b => b.Key == key);
            result.Value = this;

            return result;
        }

        public virtual IResult<IFlow> AddConnection(BlockKey from, BlockKey to, object continuationValue = null)
        {
            var result = CompositeResult<IFlow>.Ok();

            if(from == null)
            {
                result.AddIssue(IssueCodes.General.ArgumentNull, new[]
                {
                    IssueParameters.General.Argument.WithValue(nameof(from))
                });
            }

            if(to == null)
            {
                result.AddIssue(IssueCodes.General.ArgumentNull, new[]
                {
                    IssueParameters.General.Argument.WithValue(nameof(to))
                });
            }

            if(result.IsOk)
            {
                if(Connections.Any(c => c.From == from && c.To == to))
                {
                    result.AddIssue(IssueCodes.Flow.DuplicateConnection, new[]
                    {
                        IssueParameters.Flow.From.WithValue(from),
                        IssueParameters.Flow.To.WithValue(to)
                    });
                }

                if(Blocks.All(b => b.Key != from))
                {
                    result.AddIssue(IssueCodes.Flow.BlockNotFoundInCollection, new[]
                    {
                        IssueParameters.Flow.From.WithValue(from)
                    });
                }

                if(Blocks.All(b => b.Key != to))
                {
                    result.AddIssue(IssueCodes.Flow.BlockNotFoundInCollection, new[]
                    {
                        IssueParameters.Flow.To.WithValue(to)
                    });
                }

                if(result.IsOk)
                {
                    Connections.Add(new BlockKeyConnection(from, to, continuationValue));
                    result.Value = this;
                }
            }

            return result;
        }

        public virtual IResult<IFlow> RemoveConnection(BlockKey from, BlockKey to)
        {
            var result = CompositeResult<IFlow>.Ok();

            Connections.RemoveAll(c => c.From == from && c.To == to);
            result.Value = this;

            return result;
        }

        #endregion IFlow
    }
}