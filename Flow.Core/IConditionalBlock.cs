﻿using System;
using System.Collections.Generic;

namespace Flow.Core
{
    public interface IConditionalBlock : IBlock
    {
        object Evaluate();
        HashSet<object> ConnectionPoints { get; }
    }
}