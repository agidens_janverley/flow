﻿namespace Flow.Core
{
    public class Connection<T> : IConnection<T>
    {
        public virtual T From { get; }
        public virtual T To { get; }
        public virtual object ContinuationValue { get; }

        public Connection(T from, T to, object continuationValue)
        {
            From = from;
            To = to;
            ContinuationValue = continuationValue;
        }
    }

    public class BlockKeyConnection : Connection<BlockKey>
    {
        public BlockKeyConnection(BlockKey from, BlockKey to, object continuationValue = null)
            : base(from, to, continuationValue)
        {
        }
    }

    public class BlockConnection : Connection<IBlock>
    {
        public BlockConnection(IBlock from, IBlock to, object continuationValue = null)
            : base(from, to, continuationValue)
        {
        }
    }
}