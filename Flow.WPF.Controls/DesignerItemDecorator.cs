﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace Flow.WPF.Controls
{
    public class DesignerItemDecorator : Control
    {
        private Adorner adorner;

        public bool ShowDecorator
        {
            get => (bool)GetValue(ShowDecoratorProperty);
            set => SetValue(ShowDecoratorProperty, value);
        }

        public static readonly DependencyProperty ShowDecoratorProperty =
            DependencyProperty.Register(nameof(ShowDecorator), typeof(bool), typeof(DesignerItemDecorator), new PropertyMetadata(false, ShowDecoratorProperty_Changed));

        private void HideAdorner()
        {
            if(adorner != null)
                adorner.Visibility = Visibility.Hidden;
        }

        private void ShowAdorner()
        {
            if(adorner == null)
            {
                var adornerLayer = AdornerLayer.GetAdornerLayer(this);

                if(adornerLayer == null)
                    return;

                if(!(DataContext is ContentControl designerItem))
                    return;

                var canvas = VisualTreeHelper.GetParent(designerItem) as Canvas;
                //this.adorner = new ResizeRotateAdorner(designerItem);
                adornerLayer.Add(adorner);

                adorner.Visibility = ShowDecorator
                    ? Visibility.Visible
                    : Visibility.Hidden;
            }
            else
                adorner.Visibility = Visibility.Visible;
        }

        private static void ShowDecoratorProperty_Changed
            (DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(!(d is DesignerItemDecorator decorator))
                return;

            var showDecorator = (bool)e.NewValue;

            if(showDecorator)
                decorator.ShowAdorner();
            else
                decorator.HideAdorner();
        }
    }
}