﻿using System.IO;
using System.Xml.Linq;

namespace Flow.IO.Xml.Extensions
{
    public static class XDocumentExtensions
    {
        public static byte[] AsByteArray(this XDocument xml)
        {
            using(var memStream = new MemoryStream())
            {
                xml.Save(memStream);

                return memStream.ToArray();
            }
        }
    }
}