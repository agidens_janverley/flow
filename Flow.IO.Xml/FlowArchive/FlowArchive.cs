﻿using System.Xml.Linq;

namespace Flow.IO.Xml.FlowArchive
{
    public class FlowArchive : IFlowArchive
    {
        public string Name { get; set; }
        public XDocument FlowFile { get; }
        public XDocument DesignerFile { get; }

        public FlowArchive(string name, XDocument flowFile, XDocument designerFile)
        {
            Name = name;
            FlowFile = flowFile;
            DesignerFile = designerFile;
        }
    }
}