﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Flow.Core.Result;
using Ionic.Zip;

namespace Flow.IO.Xml.FlowArchive
{
    public class FlowArchiveReader : FlowArchiveManipulator, IFlowArchiveReader
    {
        public IResult<IFlowArchive> Load(string path)
        {
            var result = CompositeResult<IFlowArchive>.Ok();

            try
            {
                if(string.IsNullOrWhiteSpace(path))
                {
                    result.AddIssue(IssueCodes.General.EmptyString, new []
                    {
                        IssueParameters.General.EmptyString.WithValue(nameof(path))
                    });
                }

                if(result.IsOk)
                {
                    if(!Path.GetExtension(path)?.Equals(FlowArchiveExtension, StringComparison.InvariantCultureIgnoreCase) ?? false)
                    {
                        result.AddIssue(IssueCodes.FlowArchiveReader.IncorrectExtension, new[]
                        {
                            IssueParameters.FlowArchiveReader.Path.WithValue(nameof(path)),
                            IssueParameters.FlowArchiveReader.ActualExtension.WithValue(Path.HasExtension(path) ? Path.GetExtension(path) : string.Empty),
                            IssueParameters.FlowArchiveReader.ExpectedExtension.WithValue(FlowArchiveExtension)
                        });
                    }

                    if(result.IsOk)
                    {
                        var name = Path.GetFileNameWithoutExtension(path);
                        var flowFileName = $"{name}{FlowFileExtension}";
                        var designerFileName = $"{name}{DesignerFileExtension}";

                        using(var zip = new ZipFile(path))
                        {
                            if(!zip.EntryFileNames.Any(n => n.Equals(flowFileName, StringComparison.CurrentCultureIgnoreCase)))
                            {
                                result.AddIssue(IssueCodes.FlowArchiveReader.ArchiveDoesNotContaineFlowFile, new[]
                                {
                                    IssueParameters.FlowArchiveReader.Path.WithValue(path),
                                    IssueParameters.FlowArchiveReader.Filename.WithValue(flowFileName)
                                });
                            }

                            if(!zip.EntryFileNames.Any(n => n.Equals(designerFileName, StringComparison.CurrentCultureIgnoreCase)))
                            {
                                result.AddIssue(IssueCodes.FlowArchiveReader.ArchiveDoesNotContaineDesignerFile, new[]
                                {
                                    IssueParameters.FlowArchiveReader.Path.WithValue(path),
                                    IssueParameters.FlowArchiveReader.Filename.WithValue(designerFileName)
                                });
                            }

                            var flowFile = ExtractFile(zip, flowFileName);
                            var designerFile = ExtractFile(zip, designerFileName);

                            result.Value = new FlowArchive(name, flowFile, designerFile);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                result.AddIssue(IssueCodes.General.Unexpected, new[]
                {
                    IssueParameters.General.Exception.WithValue(e)
                });
            }

            return result;
        }

        private XDocument ExtractFile(ZipFile archive, string filename)
        {
            if(!archive.ContainsEntry(filename))
                throw new ArgumentException(nameof(filename));

            using(var ms = new MemoryStream())
            {
                var entry = archive.Entries.Single(e => e.FileName.Equals(filename, StringComparison.CurrentCultureIgnoreCase));
                entry.Extract(ms);
                ms.Position = 0;

                return XDocument.Load(ms);
            }
        }
    }
}