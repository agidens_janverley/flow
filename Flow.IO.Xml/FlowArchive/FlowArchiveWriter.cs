﻿using System;
using System.IO;
using Flow.Core.Result;
using Flow.IO.Xml.Extensions;
using Ionic.Zip;

namespace Flow.IO.Xml.FlowArchive
{
    public class FlowArchiveWriter : FlowArchiveManipulator, IFlowArchiveWriter
    {
        public IResult<IFlowArchive> Save(IFlowArchive flowArchive, string path)
        {
            var result = Result<IFlowArchive>.Ok(flowArchive);

            try
            {
                if(flowArchive == null)
                {
                    result.AddIssue(IssueCodes.General.ArgumentNull, new[]
                    {
                        IssueParameters.General.Argument.WithValue(nameof(flowArchive))
                    });
                }

                if(result.IsOk)
                {
                    if(string.IsNullOrWhiteSpace(flowArchive.Name))
                    {
                        result.AddIssue(IssueCodes.General.ArgumentNull, new[]
                        {
                            IssueParameters.General.Argument.WithValue(nameof(flowArchive.Name))
                        });
                    }

                    if(flowArchive.DesignerFile == null)
                    {
                        result.AddIssue(IssueCodes.General.ArgumentNull, new[]
                        {
                            IssueParameters.General.Argument.WithValue(nameof(flowArchive.DesignerFile))
                        });
                    }

                    if(flowArchive.FlowFile == null)
                    {
                        result.AddIssue(IssueCodes.General.ArgumentNull, new[]
                        {
                            IssueParameters.General.Argument.WithValue(nameof(flowArchive.FlowFile))
                        });
                    }

                    if(string.IsNullOrWhiteSpace(path))
                    {
                        result.AddIssue(IssueCodes.General.EmptyString, new[]
                        {
                            IssueParameters.General.EmptyString.WithValue(nameof(path))
                        });
                    }

                    if(result.IsOk)
                    {
                        var flowFileName = $"{flowArchive.Name}{FlowFileExtension}";
                        var designerFileName = $"{flowArchive.Name}{DesignerFileExtension}";

                        using(var zip = new ZipFile(Path.Combine(path, $"{flowArchive.Name}{FlowArchiveExtension}")))
                        {
                            zip.UpdateEntry(flowFileName, flowArchive.FlowFile.AsByteArray());
                            zip.UpdateEntry(designerFileName, flowArchive.DesignerFile.AsByteArray());
                            zip.Save();
                        }
                    }
                }
            }
            catch(Exception e)
            {
                result.AddIssue(IssueCodes.General.Unexpected, new[]
                {
                    IssueParameters.General.Exception.WithValue(e)
                });
            }

            return result;
        }
    }
}