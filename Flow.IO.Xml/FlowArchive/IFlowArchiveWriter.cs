﻿using Flow.Core.Result;

namespace Flow.IO.Xml.FlowArchive
{
    public interface IFlowArchiveWriter
    {
        IResult<IFlowArchive> Save(IFlowArchive flowArchive, string path);
    }
}