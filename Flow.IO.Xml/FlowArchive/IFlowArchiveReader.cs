﻿using Flow.Core.Result;

namespace Flow.IO.Xml.FlowArchive
{
    public interface IFlowArchiveReader
    {
        IResult<IFlowArchive> Load(string path);
    }
}