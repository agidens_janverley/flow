﻿namespace Flow.IO.Xml
{
    public static class XmlSupportedNames
    {
        public static class TagNames
        {
            public static string Flow = "Flow";
            public static string Blocks = "Blocks";
            public static string Block = "Block";
            public static string BlockConnections = "BlockConnections";
            public static string BlockConnection = "BlockConnection";
            public static string Settings = "Settings";
            public static string Setting = "Setting";
            public static string Layout = "Layout";
            public static string Element = "Element";
            public static string Contributions = "Contributions";
            public static string Contribution = "Contribution";
            public static string Requirements = "Requirements";
            public static string Requirement = "Requirement";
        }

        public static class AttributeNames
        {
            public static string Version = "version";
            public static string Uuid = "uuid";
            public static string Type = "type";
            public static string From = "from";
            public static string To = "to";
            public static string Key = "key";
            public static string Value = "value";
            public static string X = "x";
            public static string Y = "y";
            public static string Name = "name";
            public static string Alias = "alias";
        }
    }
}