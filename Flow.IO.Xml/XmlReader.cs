﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Agidens.Logging;
using Flow.Core;
using Flow.Core.Resolvers;
using Flow.Core.Result;

namespace Flow.IO.Xml
{
    public class XmlReader : IXmlReader
    {
        private const string extension = "xml";

        private readonly ILoggerFactory loggerFactory;
        private readonly ILogger logger;
        private readonly IBlockFactory blockFactory;
        private readonly IBlockResolver blockResolver;
        private readonly IRequirementResolver requirementResolver;

        public XmlReader(IBlockFactory blockFactory, IBlockResolver blockResolver, IRequirementResolver requirementResolver, ILoggerFactory loggerFactory)
        {
            this.blockFactory = blockFactory;
            this.blockResolver = blockResolver;
            this.requirementResolver = requirementResolver;
            this.loggerFactory = loggerFactory;
            logger = loggerFactory?.GetLogger(this);
        }

        public IResult<IFlow> Load(string uri)
        {
            var result = CompositeResult<IFlow>.Ok();

            if(uri == null)
            {
                result.AddIssue(IssueCodes.General.ArgumentNull, new[]
                {
                    IssueParameters.General.Argument.WithValue(nameof(uri))
                });
            }
            
            if(result.IsOk)
            {
                if(!File.Exists(uri))
                {
                    result.AddIssue(IssueCodes.XmlReader.FileDoesNotExist, new[]
                    {
                        IssueParameters.XmlReader.File.WithValue(uri)
                    });
                }
                else
                {
                    var actualExtension = Path.GetExtension(uri);

                    if(!actualExtension?.Equals($".{extension}", StringComparison.InvariantCultureIgnoreCase) ?? true)
                    {
                        result.AddIssue(IssueCodes.XmlReader.IncorrectExtension, new[]
                        {
                            IssueParameters.XmlReader.Path.WithValue(uri),
                            IssueParameters.XmlReader.ActualExtension.WithValue(actualExtension),
                            IssueParameters.XmlReader.ExpectedExtension.WithValue(extension)
                        });
                    }
                    else
                    {
                        try
                        {
                            var xDoc = XDocument.Load(uri);
                            result.AddResult(Load(xDoc));
                        }
                        catch(Exception e)
                        {
                            result.AddIssue(IssueCodes.General.Unexpected, new[]
                            {
                                IssueParameters.General.Exception.WithValue(e)
                            });
                        }
                    }
                }
            }

            return result;
        }

        public IResult<IFlow> Load(XDocument xml)
        {
            var result = CompositeResult<IFlow>.Ok();

            try
            {
                var flowElement = GetFlow(xml);
                var blocks = GetBlocks(flowElement.Element(XmlSupportedNames.TagNames.Blocks));
                var blockConnections = GetBlockConnections(flowElement.Element(XmlSupportedNames.TagNames.BlockConnections));

                IFlow flow = new Core.Flow(blockResolver, requirementResolver, loggerFactory);

                foreach(var block in blocks)
                {
                    var createBlockResult = blockFactory.CreateBlock(block.Attribute(XmlSupportedNames.AttributeNames.Type)?.Value);
                    result.AddResult(createBlockResult);

                    if(result.IsOk)
                    {
                        var blockInstance = createBlockResult.Value;
                        var settings = GetBlockSettings(block.Element(XmlSupportedNames.TagNames.Settings));

                        if(settings != null)
                        {
                            foreach(var setting in settings)
                            {
                                var processedSetting = ProcessBlockSetting(setting);

                                if(blockInstance.Settings.Any(s => s.Key == processedSetting.Key))
                                {
                                    blockInstance.SetSettingValue(processedSetting.Key, processedSetting.Value);
                                }
                            }
                        }

                        var requirements = GetBlockRequirements(block.Element(XmlSupportedNames.TagNames.Requirements));

                        if(requirements != null)
                        {
                            foreach(var requirement in requirements)
                            {
                                ProcessBlockRequirement(blockInstance, requirement);
                            }
                        }

                        var contributions = GetBlockContributions(block.Element(XmlSupportedNames.TagNames.Contributions));

                        if(contributions != null)
                        {
                            foreach(var contribution in contributions)
                            {
                                ProcessBlockContribution(blockInstance, contribution);
                            }
                        }

                        flow.AddBlock(new BlockKey(ParseAttributeValueAsGuid(block.Attribute(XmlSupportedNames.AttributeNames.Uuid))), blockInstance);
                    }
                }

                if(result.IsOk)
                {
                    foreach(var blockConnection in blockConnections)
                    {
                        var connection = ProcessBlockConnection(blockConnection);
                        flow.AddConnection(connection.From, connection.To);
                    }
                }

                result.Value = flow;
            }
            catch(Exception e)
            {
                result.AddIssue(IssueCodes.General.Unexpected, new[]
                {
                    IssueParameters.General.Exception.WithValue(e)
                });
            }

            return result;
        }

        private XElement GetFlow(XDocument xml) => xml?.Descendants().SingleOrDefault(p => p.Name.LocalName == XmlSupportedNames.TagNames.Flow);
        private string DetermineVersion(XDocument xml) => xml?.Root?.Attribute(XmlSupportedNames.AttributeNames.Version)?.Value;
        private IEnumerable<XElement> GetBlocks(XElement element) => element?.Descendants(XmlSupportedNames.TagNames.Block);
        private IEnumerable<XElement> GetBlockConnections(XElement element) => element?.Descendants(XmlSupportedNames.TagNames.BlockConnection);
        private IEnumerable<XElement> GetBlockSettings(XElement element) => element?.Descendants(XmlSupportedNames.TagNames.Setting);
        private IEnumerable<XElement> GetBlockRequirements(XElement element) => element?.Descendants(XmlSupportedNames.TagNames.Requirement);
        private IEnumerable<XElement> GetBlockContributions(XElement element) => element?.Descendants(XmlSupportedNames.TagNames.Contribution);

        private Guid ParseAttributeValueAsGuid(XAttribute attribute)
        {
            return Guid.TryParse(attribute?.Value, out var guid)
                ? guid
                : Guid.Empty;
        }

        private IConnection<BlockKey> ProcessBlockConnection(XElement element)
        {
            if(element == null)
                return null;

            var from = new BlockKey(ParseAttributeValueAsGuid(element.Attribute(XmlSupportedNames.AttributeNames.From)));
            var to = new BlockKey(ParseAttributeValueAsGuid(element.Attribute(XmlSupportedNames.AttributeNames.To)));

            var connection = new BlockKeyConnection(from, to);

            return connection;
        }

        private ISetting ProcessBlockSetting(XElement element)
        {
            if(element == null)
                return null;

            var key = element.Attribute(XmlSupportedNames.AttributeNames.Key)?.Value;
            var value = element.Attribute(XmlSupportedNames.AttributeNames.Value)?.Value;

            var setting = new Setting(key, value);

            return setting;
        }

        private void ProcessBlockRequirement(IBlock blockInstance, XElement element)
        {
            if(element.Name != XmlSupportedNames.TagNames.Requirement)
                throw new ArgumentException(nameof(element));

            var name = element.Attribute(XmlSupportedNames.AttributeNames.Name)?.Value;
            var alias = element.Attribute(XmlSupportedNames.AttributeNames.Alias)?.Value;

            var requirement = blockInstance.Requirements.SingleOrDefault(r => r.Property.Name == name);

            if(requirement != null)
                requirement.Alias = alias;
        }

        private void ProcessBlockContribution(IBlock blockInstance, XElement element)
        {
            if(element.Name != XmlSupportedNames.TagNames.Contribution)
                throw new ArgumentException(nameof(element));

            var name = element.Attribute(XmlSupportedNames.AttributeNames.Name)?.Value;
            var alias = element.Attribute(XmlSupportedNames.AttributeNames.Alias)?.Value;

            var contribution = blockInstance.Contributions.SingleOrDefault(r => r.Property.Name == name);

            if(contribution != null)
                contribution.Alias = alias;
        }
    }
}