﻿using Agidens.Terminal.Suite.Service.OrderManager.Client;
using Agidens.Terminal.Suite.Service.OrderManager.Contract;
using Aline.Resource.Client;
using Aline.Resource.Contract;

namespace Flow.Kiosk.Shell
{
    public class ServiceFactory : IServiceFactory
    {
        public IIdentificationManager CreateIdentificationManager()
        {
            return new IdentificationClient("NetTcpBinding_IIdentificationManager").Proxy;
        }

        public IWorkOrderManager CreateWorkOrderManager()
        {
            return new WorkOrderClient("NetTcpBinding_IWorkOrderManager").Proxy;
        }

        public ISalesOrderManager CreateSalesOrderManager()
        {
            return new SalesOrderClient("NetTcpBinding_ISalesOrderManager").Proxy;
        }

        public IPersonManager CreatePersonManager()
        {
            return new PersonClient("NetTcpBinding_IPersonManager").Proxy;
        }

        public ISystemManager CreateSystemManager()
        {
            return new SystemClient("NetTcpBinding_ISystemManager").Proxy;
        }

        public ITranslationManager CreateTranslationManager()
        {
            return new ResourceClient("NetTcpBinding_ITranslationManager").Proxy;
        }
    }
}