﻿using Agidens.Terminal.Suite.Service.OrderManager.Contract;
using Aline.Resource.Contract;

namespace Flow.Kiosk.Shell
{
    public interface IServiceFactory
    {
        IIdentificationManager CreateIdentificationManager();
        IWorkOrderManager CreateWorkOrderManager();
        ISalesOrderManager CreateSalesOrderManager();
        IPersonManager CreatePersonManager();
        ISystemManager CreateSystemManager();
        ITranslationManager CreateTranslationManager();
    }
}