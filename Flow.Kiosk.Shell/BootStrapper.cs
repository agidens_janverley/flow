﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using Agidens.Logging;
using Agidens.Terminal.Suite.Service.OrderManager.Contract;
using Aline.Resource.Contract;
using Caliburn.Micro;
using Flow.Core;
using Flow.Core.Resolvers;
using Flow.IO.Resolvers;
using Flow.IO.Xml;
using Flow.IO.Xml.FlowArchive;
using Flow.Kiosk.Shared;
using Flow.Kiosk.Shell.Resolvers;
using NDesk.Options;
using Ninject;

namespace Flow.Kiosk.Shell
{
    public class BootStrapper : BootstrapperBase
    {
        private CompositionContainer container;
        private static readonly Agidens.Logging.ILogger Logger;
        private static readonly Agidens.Logging.ILoggerFactory LoggerFactory;
        private static readonly IBlockResolver BlockResolver;
        private static readonly IFlowArchiveReader FlowArchiveReader;
        private static readonly IXmlReader XmlReader;
        private static readonly IFlowRunner FlowRunner;
        private static readonly IAssemblyResolver AssemblyResolver;

        private static readonly IBlocksResolver ModelResolver;
        private static readonly IBlockFactory ModelFactory;
        private static readonly IViewModelResolver ViewModelResolver;
        private static readonly IViewModelFactory ViewmodelFactory;
        private static readonly IKernel Kernel;
        private static readonly IShellSettings ShellSettings;
        private static readonly IServiceFactory ServiceFactory;

        private bool noSplash = false;
        private string flowFilename;
#if DEBUG
        private string applicationConfigFileName = "";
#endif

        static BootStrapper()
        {
#if DEBUG
            Debugger.Launch();
#endif

            Kernel = new StandardKernel();
            Kernel.Load("NinjectConfig.xml");
            LoggerFactory = Kernel.Get<Agidens.Logging.ILoggerFactory>();
            Logger = LoggerFactory?.GetLogger(typeof(BootStrapper).FullName);

            Logger?.Log(LogLevels.Debug, "===== Resolving Dependencies =====");

            //Initialse assemblies
            AssemblyResolver = Kernel.Get<IAssemblyResolver>();
            LogDependency(typeof(IAssemblyResolver), AssemblyResolver?.GetType());

            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var assemblies = AssemblyResolver.Resolve(path)?.ToList();

            //Initialise services
            ServiceFactory = Kernel.Get<IServiceFactory>();
            Kernel.Bind<IIdentificationManager>().ToMethod(context => ServiceFactory.CreateIdentificationManager());
            Kernel.Bind<IWorkOrderManager>().ToMethod(context => ServiceFactory.CreateWorkOrderManager());
            Kernel.Bind<ISalesOrderManager>().ToMethod(context => ServiceFactory.CreateSalesOrderManager());
            Kernel.Bind<IPersonManager>().ToMethod(context => ServiceFactory.CreatePersonManager());
            Kernel.Bind<ISystemManager>().ToMethod(context => ServiceFactory.CreateSystemManager());
            Kernel.Bind<ITranslationManager>().ToMethod(context => ServiceFactory.CreateTranslationManager());

            //Resolve blocks
            ModelResolver = Kernel.Get<IBlocksResolver>();
            LogDependency(typeof(IBlocksResolver), ModelResolver?.GetType());
            var models = ModelResolver.Resolve(assemblies);

            foreach(var model in models)
            {
                Kernel.Bind(typeof(IBlock)).To(model).Named($"{model.FullName ?? model.Name}");
                Logger?.Log(LogLevels.Debug, $"Binding IBlock to {model.FullName ?? model.Name}");
            }

            ModelFactory = Kernel.Get<IBlockFactory>();
            LogDependency(typeof(IBlockFactory), ModelFactory?.GetType());

            //Resolve viewmodels
            ViewModelResolver = Kernel.Get<IViewModelResolver>();
            LogDependency(typeof(IViewModelResolver), ViewModelResolver?.GetType());

            var viewModels = ViewModelResolver.Resolve(assemblies);

            foreach(var viewModel in viewModels)
            {
                Kernel.Bind<IViewModel<IBlock>>().To(viewModel).Named(viewModel.FullName ?? viewModel.Name);
                Logger?.Log(LogLevels.Debug, $"Binding IViewModel<IBlock> to {viewModel.FullName ?? viewModel.Name}");
            }

            ViewmodelFactory = Kernel.Get<IViewModelFactory>();
            LogDependency(typeof(IViewModelFactory), ViewmodelFactory?.GetType());

            BlockResolver = Kernel.Get<IBlockResolver>();
            LogDependency(typeof(IBlockResolver), BlockResolver?.GetType());

            FlowArchiveReader = Kernel.Get<IFlowArchiveReader>();
            LogDependency(typeof(IFlowArchiveReader), FlowArchiveReader?.GetType());

            XmlReader = Kernel.Get<IXmlReader>();
            LogDependency(typeof(IXmlReader), XmlReader?.GetType());

            FlowRunner = Kernel.Get<IFlowRunner>();
            LogDependency(typeof(IFlowRunner), FlowRunner?.GetType());

            ShellSettings = Kernel.Get<IShellSettings>();
            LogDependency(typeof(IShellSettings), ShellSettings?.GetType());
        }

        public BootStrapper()
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
            {
                Logger.Log(LogLevels.Error, "Unhandled exception!", (Exception)args.ExceptionObject);
            };
            // Initialize the Caliburn framework
            Initialize();
        }

        protected override void Configure()
        {
            var cat = new AggregateCatalog(AssemblySource.Instance.Select(x => new AssemblyCatalog(x)));
            container = new CompositionContainer(cat);

            var batch = new CompositionBatch();
            batch.AddExportedValue<IWindowManager>(new WindowManager());
            batch.AddExportedValue<IEventAggregator>(new EventAggregator());
            batch.AddExportedValue<ILoggerFactory>(LoggerFactory);
            batch.AddExportedValue<IXmlReader>(XmlReader);
            batch.AddExportedValue<IViewModelFactory>(ViewmodelFactory);
            batch.AddExportedValue<IFlowRunner>(FlowRunner);
            batch.AddExportedValue<IShellSettings>(ShellSettings);
            batch.AddExportedValue<IFlowArchiveReader>(FlowArchiveReader);
            container.Compose(batch);
        }

        protected override object GetInstance(Type service, string key)
        {
            var contract = string.IsNullOrEmpty(key) ? AttributedModelServices.GetContractName(service) : key;
            var exports = container.GetExportedValues<object>(contract).ToList();

            if(exports.Any())
                return exports.First();

            throw new Exception($"Could not locate any instances of contract {contract}.");
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return container.GetExportedValues<object>(AttributedModelServices.GetContractName(service));
        }

        protected override void BuildUp(object instance)
        {
            container.SatisfyImportsOnce(instance);
        }

        protected override IEnumerable<Assembly> SelectAssemblies()
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var baseAssemblies = new List<Assembly>(base.SelectAssemblies());
            var assemblies = AssemblyResolver.Resolve(path);

            foreach(var thisAssembly in assemblies)
            {
                if(!baseAssemblies.Contains(thisAssembly))
                {
                    baseAssemblies.Add(thisAssembly);
                }

                // If this library is being accessed from a Caliburn enabled app then
                // this assembly may already be 'known' in the AssemblySource.Instance collection.
                // We need to remove these otherwise we'll get:
                //  "An item with the same key has already been added." (System.ArgumentException)
                // which (for my scenario) eventually manifested itself as a:
                //  "" (System.ComponentModel.Composition.CompositionException)
                foreach(var assembly in baseAssemblies.ToList().Where(newAssembly => AssemblySource.Instance.Contains(newAssembly)))
                {
                    baseAssemblies.Remove(assembly);
                }
            }

            return baseAssemblies;
        }

        protected override void OnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            Logger.Log(LogLevels.Error, "Unhandled exception!", e.Exception);
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            try
            {
                Logger?.Log(LogLevels.Info, "====== Starting application ======");
                //set default culture as installed culture of machine
                Thread.CurrentThread.CurrentCulture = CultureInfo.InstalledUICulture;
                Thread.CurrentThread.CurrentUICulture = CultureInfo.InstalledUICulture;

                HandleArguments();

#if DEBUG
                if(!string.IsNullOrEmpty(applicationConfigFileName))
                {
                    Logger?.Log(LogLevels.Info, $"Application configuration file specified: {applicationConfigFileName}...");

                    var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    configuration.AppSettings.File = applicationConfigFileName;
                    configuration.Save(ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection("appSettings");
                }
#endif

                //open splash screen
                var splash = new SplashScreen(@"\Resources/logo_agidens.png");

                if(!noSplash)
                {
                    splash.Show(false, true);
                }
                else
                {
                    Logger?.Log(LogLevels.Info, "Splashscreen not shown: Command Line Argument noSplash");
                }
                
                if(ShellSettings != null
                   && flowFilename != null)
                {
                    ShellSettings.FlowFilename = flowFilename;
                }
                
                DisplayRootViewFor<IShell>();

                splash.Close(new TimeSpan(0, 0, 1));
                Logger?.Log(LogLevels.Info, "====== Application started ======");

            }
            catch(AgidensUIException ex)
            {
                Logger?.Log(LogLevels.Error, ex.Message, ex);
                Application.Current.Shutdown(-1);
            }
            catch(OptionException ex)
            {
                Logger?.Log(LogLevels.Error, ex.Message, ex);
                Application.Current.Shutdown(-1);
            }
            catch(Exception ex)
            {
                Logger?.Log(LogLevels.Error, ex.Message, ex);
                Application.Current.Shutdown(-1);
            }
        }

        private void HandleArguments()
        {
            var optionSet = new OptionSet
            {
#if  DEBUG             
                {
                    "c|config",
                    $"Specify the Application Config file.{Environment.NewLine}Default when not specified: default Application Config",
                    v => {if (v != null) { applicationConfigFileName = v;}}
                },
#endif
                {
                    "n|nosplash",
                    $"Show NO Splashscreen.{Environment.NewLine}Default when not specified: show Splashscreen",
                    v => noSplash = v != null
                },
                {
                    "f|flow=",
                    "Name of the Flow-file to execute.",
                    v => {if (v != null) { flowFilename = v;}}
                },
            };

            Logger?.Log(LogLevels.Info, "Parsing Command Line Arguments...");

            optionSet.Parse(Environment.GetCommandLineArgs());
#if DEBUG
            Logger?.Log(LogLevels.Info, $"config: {applicationConfigFileName}");
#endif
            Logger?.Log(LogLevels.Info, $"{nameof(noSplash)}: {noSplash.ToString()}");
            Logger?.Log(LogLevels.Info, $"flow: {flowFilename}");
            Logger?.Log(LogLevels.Info, "Done Parsing Command Line Arguments");
        }

        private static void LogDependency(Type toResolve, Type resolved)
        {
            Logger?.Log(LogLevels.Debug, $"Resolved {toResolve?.FullName} to {resolved?.FullName}");
        }
    }
}