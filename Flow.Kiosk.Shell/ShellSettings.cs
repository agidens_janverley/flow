﻿using System;
using System.Configuration;
using System.Globalization;
using Agidens.Logging;

namespace Flow.Kiosk.Shell
{
    public class ShellSettings : IShellSettings
    {
        private const string defaultCulture = "en-GB";
        private readonly ILoggerFactory loggerFactory;
        private readonly ILogger logger;

        public CultureInfo DefaultCulture { get; private set; }
        public string FlowFilename { get; set; }

        public ShellSettings(ILoggerFactory loggerFactory)
        {
            this.loggerFactory = loggerFactory;
            logger = this.loggerFactory?.GetLogger(this);

            InitialiseSettings();
        }

        private void InitialiseSettings()
        {
            FlowFilename = ConfigurationManager.AppSettings[nameof(FlowFilename)];
            DefaultCulture = new CultureInfo(defaultCulture);
            var culture = ConfigurationManager.AppSettings[nameof(DefaultCulture)];

            if(!string.IsNullOrWhiteSpace(culture))
            {
                try
                {
                    DefaultCulture = new CultureInfo(culture);
                }
                catch(Exception e)
                {
                    logger?.Log(LogLevels.Error, e.Message, e);
                }
            }
        }
    }
}