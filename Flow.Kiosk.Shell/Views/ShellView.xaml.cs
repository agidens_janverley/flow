﻿using System.Configuration;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Flow.Kiosk.Shell.Views
{
    /// <summary>
    /// Interaction logic for ShellView.xaml
    /// </summary>
    public partial class ShellView : UserControl
    {
        public ShellView()
        {
            InitializeComponent();
            EventManager.RegisterClassHandler(typeof(TextBox), TextBox.GotFocusEvent, new RoutedEventHandler(GotFocus));
            Keyboard.KeyTriggered += KeyboardOnKeyTriggered;
        }

        private void KeyboardOnKeyTriggered(Shared.Controls.Keyboard keyboard, Shared.Controls.KeyEventArgs args)
        {
            if(keyboard?.LinkedControl != null
               && args?.Key != null)
            {
                var keyValue = args.Key.TextValue ?? args.Key.DisplayText;

                if(keyboard.LinkedControl is TextBox textBox)
                {
                    if(args.Key.Value == Key.Back)
                    {
                        textBox.Text = HandleBackKey(textBox.Text);
                    }
                    else if(args.Key.Value == Key.Next)
                    {
                        keyboard.Version += 1;
                    }
                    else if(args.Key.Value == Key.Prior)
                    {
                        keyboard.Version -= 1;
                    }
                    else
                    {
                        textBox.Text += keyValue;
                    }
                }
            }
        }

        public void GotFocus(object sender, RoutedEventArgs args)
        {
            if(!(sender is TextBox textBox))
            {
                Keyboard.Visibility = Visibility.Collapsed;
                return;
            }

            var keyboardConfig = Shared.AttachedProperties.Keyboard.GetKeyboardConfiguration(textBox);
            var keyboardConfigSetting = Shared.AttachedProperties.Keyboard.GetKeyboardConfigurationSetting(textBox);

            Keyboard.KeyBoardConfig = keyboardConfig ?? (ConfigurationManager.AppSettings.AllKeys.Contains(keyboardConfigSetting)
                                          ? ConfigurationManager.AppSettings[keyboardConfigSetting]
                                          : null);
            Keyboard.Visibility = string.IsNullOrWhiteSpace(Keyboard.KeyBoardConfig)
                ? Visibility.Collapsed
                : Visibility.Visible;
            Keyboard.LinkedControl = textBox;

            args.Handled = true;
        }

        #region Helpers

        private string HandleBackKey(string text)
        {
            return (!string.IsNullOrEmpty(text))
                ? text.Substring(0, text.Length - 1)
                : text;
        }

        #endregion Helpers
    }
}