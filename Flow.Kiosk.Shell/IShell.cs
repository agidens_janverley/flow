﻿using Agidens.Logging;

namespace Flow.Kiosk.Shell
{
    public interface IShell
    {
        ILoggerFactory LoggerFactory { get; }
        ILogger Logger { get; }
        IShellSettings ShellSettings { get; }
    }
}