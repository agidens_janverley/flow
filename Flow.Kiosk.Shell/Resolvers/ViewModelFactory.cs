using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Agidens.Logging;
using Flow.Core;
using Flow.Kiosk.Shared;
using Flow.Kiosk.Shared.Attributes;
using Ninject;
using Ninject.Activation;
using Ninject.Activation.Caching;
using Ninject.Parameters;
using Ninject.Planning;
using Ninject.Planning.Bindings;

namespace Flow.Kiosk.Shell.Resolvers
{
    public class ViewModelFactory : IViewModelFactory
    {
        private readonly ILoggerFactory loggerFactory;
        private readonly ILogger logger;
        private readonly IKernel kernel;
        private readonly List<Type> types;

        public ViewModelFactory(IKernel kernel, ILoggerFactory loggerFactory)
        {
            this.kernel = kernel;
            this.loggerFactory = loggerFactory;
            logger = loggerFactory?.GetLogger(this);

            logger?.Log(LogLevels.Debug, $"Resolving {nameof(ViewModelFactory)} types.");
            types = GetBoundToTypes(typeof(IViewModel<IBlock>))?.ToList();
            logger?.Log(LogLevels.Debug, $"Resolved {nameof(ViewModelFactory)} types: {string.Join(", ", types?.OrderBy(t => t.FullName)?.Select(t => t.FullName) ?? Enumerable.Empty<string>())}");
        }

        public IViewModel<IBlock> CreateViewModel(IBlock model)
        {
            logger?.Log(LogLevels.Debug, $"Creating view model for '{model?.Type}'");
            IViewModel<IBlock> viewModel = null;
            Type typeToConstruct = null;

            if(model != null)
            {
                var typesToConsider = types.Where(t => t.GetInterfaces().Any(i => i.GenericTypeArguments.Any(gta => gta == model.GetType()))).ToList();
                logger?.Log(LogLevels.Debug, $"Types to consider: {string.Join(", ", typesToConsider?.OrderBy(t => t.FullName)?.Select(t => t.FullName))}");

                if(typesToConsider.Any())
                {
                    //If there is only 1 option, there is no need to take its priority into account.
                    if(typesToConsider.Count() == 1)
                    {
                        typeToConstruct = typesToConsider.Single();
                    }
                    //When there is more than 1 option, get the option with the highest priority.
                    else
                    {
                        logger?.Log(LogLevels.Debug, $"{typesToConsider.Count} types are available to use; selection will be based on priority-attribute.");
                        typeToConstruct = typesToConsider.Select(t => new
                            {
                                Priority = (t.GetCustomAttribute(typeof(PriorityAttribute)) is PriorityAttribute priorityAttribute)
                                    ? priorityAttribute.Priority 
                                    : int.MinValue,
                                Type = t
                            }).OrderByDescending(t => t.Priority)
                            .FirstOrDefault()
                            ?.Type;
                    }
                }
                else
                {
                    logger?.Log(LogLevels.Error, "Type could not be selected for construction.");
                }
            }

            if(typeToConstruct != null)
            {
                logger?.Log(LogLevels.Debug, $"Type '{typeToConstruct.FullName}' selected for construction.");
                viewModel = kernel?.TryGet<IViewModel<IBlock>>(typeToConstruct.FullName ?? typeToConstruct.Name);

                if(viewModel != null)
                {
                    logger?.Log(LogLevels.Error, $"Constructed type '{typeToConstruct.FullName}'.");
                    viewModel.SetModel(model);
                }
                else
                {
                    logger?.Log(LogLevels.Error, $"Failed to construct type '{typeToConstruct.FullName}'.");
                }
            }

            return viewModel;
        }

        public Type GetBoundToType(IBinding binding, Type boundType)
        {
            if(binding != null
               && kernel != null)
            {
                if(binding.Target != BindingTarget.Type
                   && binding.Target != BindingTarget.Self)
                {
                    logger?.Log(LogLevels.Warn, $"Cannot find the type to which {boundType} is bound to, because it is bound using a method, provider or constant.");

                    return null;
                }
                else
                {
                    var req = kernel.CreateRequest(boundType, metadata => true, new IParameter[0], true, false);
                    var cache = kernel.Components.Get<ICache>();
                    var planner = kernel.Components.Get<IPlanner>();
                    var pipeline = kernel.Components.Get<IPipeline>();
                    var provider = binding.GetProvider(new Context(kernel, req, binding, cache, planner, pipeline));

                    return provider.Type;
                }
            }

            if(boundType.IsClass
               && !boundType.IsAbstract)
            {
                return boundType;
            }

            logger?.Log(LogLevels.Warn, $"Cannot find the type to which {boundType} is bound to.");

            return null;
        }

        public IEnumerable<Type> GetBoundToTypes(Type boundType)
        {
            var result = new List<Type>();
            var bindings = kernel?.GetBindings(boundType)?.ToList();

            if(bindings != null)
            {
                foreach(var binding in bindings)
                {
                    var type = GetBoundToType(binding, boundType);

                    if(type != null)
                    {
                        result.Add(type);
                    }
                }
            }

            return result;
        }
    }
}