using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Flow.Core;
using Flow.Kiosk.Shared;

namespace Flow.Kiosk.Shell.Resolvers
{
    public class ViewModelResolver : IViewModelResolver
    {
        public virtual IEnumerable<Type> Resolve(IEnumerable<Assembly> assemblies)
        {
            var result = new List<Type>();

            if(assemblies != null)
            {
                foreach(var assembly in assemblies)
                {
                    var types = assembly.GetExportedTypes().Where(t => t.GetInterfaces().Any(i => i.Name == "IViewModel`1"
                                                                                                  && i.Namespace == typeof(IViewModel<>).Namespace
                                                                                                  && i.Module.Name == "Flow.Kiosk.Shared.dll"
                                                                                                  && i.GenericTypeArguments.Any(gta => gta.GetInterfaces()
                                                                                                      .Any(gtai => gtai.FullName == typeof(IBlock).FullName))));
                    result.AddRange(types);
                }
            }

            return result;
        }
    }
}