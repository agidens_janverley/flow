﻿using System;
using System.ComponentModel.Composition;
using Agidens.Logging;
using Caliburn.Micro;
using Flow.Core;
using Flow.IO.Xml;
using Flow.IO.Xml.FlowArchive;
using Flow.Kiosk.Shared;
using Microsoft.Win32;

namespace Flow.Kiosk.Shell.ViewModels
{
    [Export(typeof(IShell))]
    public class ShellViewModel : Conductor<IViewModel<IBlock>>.Collection.OneActive, IShell
    {
        [ImportingConstructor]
        public ShellViewModel(ILoggerFactory loggerFactory, IShellSettings shelSettings, 
            IFlowRunner flowRunner, IViewModelFactory viewModelFactory, 
            IFlowArchiveReader flowArchiveReader, IXmlReader xmlReader)
        {
            LoggerFactory = loggerFactory;
            Logger = LoggerFactory?.GetLogger(this);
            ShellSettings = shelSettings;
            FlowRunner = flowRunner;
            ViewModelFactory = viewModelFactory;
            FlowArchiveReader = flowArchiveReader;
            XmlReader = xmlReader;
        }

        public ILoggerFactory LoggerFactory { get; }
        public ILogger Logger { get; }
        private IFlowRunner FlowRunner { get; }
        private IViewModelFactory ViewModelFactory { get; }
        private IFlowArchiveReader FlowArchiveReader { get; }
        private IXmlReader XmlReader { get; }
        public IShellSettings ShellSettings { get; }
        
        protected override void OnActivate()
        {
            base.OnActivate();

            if(FlowRunner != null)
            {
                var filename = ShellSettings.FlowFilename;
                Core.Result.IResult archiveLoadResult = null;

                do
                {
                    if(filename == null)
                    {
                        filename = RequestFilename();
                    }

                    archiveLoadResult = LoadFlowArchive(filename);

                    if(!archiveLoadResult.IsOk)
                    {
                        filename = null;
                    }

                } while(!archiveLoadResult.IsOk);

                foreach(var block in FlowRunner.Flow.Blocks)
                {
                    block.Block.Activated += BlockOnActivated;
                    block.Block.Deactivated += BlockOnDeactivated;
                }

                FlowRunner.Start();
            }
        }

        private Core.Result.IResult LoadFlowArchive(string archiveName)
        {
            var flowArchiveLoadResult = FlowArchiveReader.Load(archiveName);
            Core.Result.IResult result = flowArchiveLoadResult;

            if(!flowArchiveLoadResult.IsOk)
            {
                Logger?.Log(LogLevels.Warn, flowArchiveLoadResult.ToString());
            }
            else
            {
                var flowArchive = flowArchiveLoadResult.Value;
                var flowLoadResult = XmlReader?.Load(flowArchive.FlowFile);
                result = flowLoadResult;

                if(!flowLoadResult?.IsOk ?? false)
                {
                    Logger?.Log(LogLevels.Warn, flowLoadResult.ToString());
                }
                else
                {
                    if(FlowRunner != null)
                    {
                        FlowRunner.Flow = flowLoadResult.Value;
                    }
                }
            }

            return result;
        }

        private string RequestFilename()
        {
            var ofd = new OpenFileDialog
            {
                Multiselect = false,
                DefaultExt = ".flow",
                CheckFileExists = true,
                AddExtension = true,
                Filter = "Flow file|*.flow"
            };

            return (!(ofd.ShowDialog() ?? false))
                ? null
                : ofd.FileName;
        }

        #region Events

        private void BlockOnActivated(object sender, EventArgs eventArgs)
        {
            if(!(sender is IBlock block))
                return;

            var vm = ViewModelFactory?.CreateViewModel(block);
            ActiveItem = vm;
        }

        private void BlockOnDeactivated(object sender, EventArgs eventArgs)
        {
            if(!(sender is IBlock block))
                return;

            block.Deactivated -= BlockOnDeactivated;
            
            if(FlowRunner.CanContinue().IsOk)
            {
                FlowRunner.Continue();
            }

            block.Deactivated += BlockOnDeactivated;
        }

        #endregion Events
    }
}