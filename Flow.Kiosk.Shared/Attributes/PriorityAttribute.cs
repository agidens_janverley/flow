using System;

namespace Flow.Kiosk.Shared.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class PriorityAttribute : Attribute
    {
        public int Priority { get; }
        
        public PriorityAttribute(int priority)
        {
            Priority = priority;
        }
    }
}