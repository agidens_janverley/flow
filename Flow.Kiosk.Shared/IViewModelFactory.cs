﻿using Flow.Core;

namespace Flow.Kiosk.Shared
{
    public interface IViewModelFactory
    {
        IViewModel<IBlock> CreateViewModel(IBlock model);
    }
}