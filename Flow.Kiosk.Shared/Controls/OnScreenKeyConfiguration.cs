﻿using System.Windows;
using System.Windows.Input;

namespace Flow.Kiosk.Shared.Controls
{
    public class OnScreenKeyConfiguration
    {
        public string DisplayText { get; set; }
        public Key? Value { get; set; } 
        public string TextValue { get; set; } 
        public Style Style { get; set; }
    }
}