﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Flow.Kiosk.Shared.Controls
{
	/// <summary>
	///     Interaction logic for OnScreenKey.xaml
	/// </summary>
	public partial class OnScreenKey
	{
	    private readonly Style defaultStyle;
	    private readonly OnScreenKeyConfiguration regularVersion;
	    private readonly List<OnScreenKeyConfiguration> alternateVersions;

		// Using a DependencyProperty as the backing store for DisplayText. This enables animation, styling, binding, etc...
		public static readonly DependencyProperty DisplayTextProperty =
			DependencyProperty.Register(nameof(DisplayText), typeof(string), typeof(OnScreenKey), new UIPropertyMetadata(string.Empty));

        // Using a DependencyProperty as the backing store for Value. This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueProperty =
			DependencyProperty.Register(nameof(Value), typeof(Key?), typeof(OnScreenKey), new UIPropertyMetadata(null));
		
		public static readonly DependencyProperty TextValueProperty =
			DependencyProperty.Register(nameof(TextValue), typeof(string), typeof(OnScreenKey), new UIPropertyMetadata(string.Empty));

        // Using a DependencyProperty as the backing store for Version. This enables animation, styling, binding, etc...
        public static readonly DependencyProperty VersionProperty =
            DependencyProperty.Register(nameof(Version), typeof(int), typeof(OnScreenKey), new PropertyMetadata(0));

	    public string DisplayText
		{
			get { return (string)GetValue(DisplayTextProperty); }
			set { SetValue(DisplayTextProperty, value); }
		}

		public Key? Value
		{
			get { return (Key?)GetValue(ValueProperty); }
			set { SetValue(ValueProperty, value); }
		}
		
		public string TextValue
		{
			get { return (string)GetValue(TextValueProperty); }
			set { SetValue(TextValueProperty, value); }
		}
		
        public int Version
        {
            get { return (int)GetValue(VersionProperty); }
            set
            {
                SetValue(VersionProperty, value);
                SetVersion(Version);
            }
        }

	    public int Versions => (alternateVersions?.Count ?? 0) + 1;

	    private void SetVersion(int version)
	    {
            if(version == 0)
            {
                DisplayText = regularVersion?.DisplayText;
                Value = regularVersion?.Value;
                TextValue = regularVersion?.TextValue;
                Style = regularVersion?.Style ?? defaultStyle;

                return;
            }

	        if(alternateVersions == null
                || !alternateVersions.Any()
                || version < 1
                || version > alternateVersions.Count)
	        {
	            DisplayText = string.Empty;
	            Value = null;
		        TextValue = null;
                Style = defaultStyle;
	        }
            else
	        {
                DisplayText = alternateVersions[version - 1].DisplayText;
                Value = alternateVersions[version - 1].Value;
                TextValue = alternateVersions[version - 1].TextValue;
                Style = alternateVersions[version - 1].Style ?? defaultStyle;
            }
	    }

	    public OnScreenKey()
		{
			InitializeComponent();  
		}

		public OnScreenKey(string displayText, Key? key = null, string textValue = null, Style style = null, IEnumerable<OnScreenKeyConfiguration> alternateVersions = null)
			: this()
		{
            InitializeComponent();
		    defaultStyle = FindResource("KeyboardKey") as Style;
            regularVersion = new OnScreenKeyConfiguration
            {
                DisplayText = displayText,
                Value = key,
	            TextValue = textValue,
                Style = style ?? defaultStyle
            };

		    this.alternateVersions = alternateVersions?.ToList();
            SetVersion(0);
		}
	}
}