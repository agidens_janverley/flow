﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;

namespace Flow.Kiosk.Shared.Controls
{
    /// <summary>
    ///     Interaction logic for Keyboard.xaml
    /// </summary>
    public partial class Keyboard
    {
        private static ResourceDictionary Resources;

        public static readonly DependencyProperty VersionProperty =
            DependencyProperty.Register(nameof(Version), typeof(int), typeof(Keyboard), new PropertyMetadata(0, VersionChangedCallback));

        // Using a DependencyProperty as the backing store for KeyBoardConfig.
        public static readonly DependencyProperty KeyBoardConfigProperty =
            DependencyProperty.Register(nameof(KeyBoardConfig),
                typeof(string),
                typeof(Keyboard),
                new PropertyMetadata(null, KeyBoardConfigChangedCallBack));

        public Keyboard()
        {
            Keys = new BindableCollection<BindableCollection<OnScreenKey>>();
            InitializeComponent();

            if(Execute.InDesignMode)
            {
                SetupDesignData();
            }
        }

        #region Events

        public event KeyEventHandler KeyTriggered;
        public delegate void KeyEventHandler(Keyboard keyboard, KeyEventArgs args);

        #endregion Events

        public string KeyBoardConfig
        {
            get { return (string)GetValue(KeyBoardConfigProperty); }
            set { SetValue(KeyBoardConfigProperty, value); }
        }

        public UIElement LinkedControl { get; set; }

        public int Version
        {
            get { return (int)GetValue(VersionProperty); }
            set
            {
                var maxVersion = DetermineNumberOfVersions();
                var newVersion = value;

                if(maxVersion == 0)
                {
                    newVersion = 0;
                }
                else
                {
                    while(newVersion < 0)
                    {
                        newVersion += maxVersion;
                    }

                    newVersion = newVersion % maxVersion;
                }
                
                SetValue(VersionProperty, newVersion);
            }
        }

        public BindableCollection<BindableCollection<OnScreenKey>> Keys { get; set; }

        private static void VersionChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            if(!(dependencyObject is Keyboard keyboard))
                return;

            foreach(var row in keyboard.Keys)
            {
                foreach(var key in row)
                    key.Version = (int)dependencyPropertyChangedEventArgs.NewValue;
            }
        }

        private static void KeyBoardConfigChangedCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(!(d is Keyboard keyboard))
                return;
            
            keyboard.SetupKeyboard(e.NewValue?.ToString() ?? e.Property.DefaultMetadata.ToString());
        }

        public override void BeginInit()
        {
            SetupKeyboard(KeyBoardConfig);
            base.BeginInit();
        }

        private void SetupDesignData() => SetupKeyboard(KeyBoardConfig);

        private void SetupKeyboard(string configuration)
        {
            Resources = new ResourceDictionary
                        {
                            Source = new Uri("pack://Application:,,,/Flow.Kiosk.Shared;component/Resources/Styles.xaml")
                        };
            Version = 0;
            var keys = ProcessConfiguration(configuration);

            foreach(var k in keys.SelectMany(kl => kl.Where(key => key != null)))
                k.Click += OnScreenKeyClick;

            foreach(var keyList in Keys)
            {
                foreach(var key in keyList.Where(k => k != null))
                    key.Click -= OnScreenKeyClick;
            }

            Keys?.Clear();
            Keys.AddRange(keys);
        }

        private BindableCollection<BindableCollection<OnScreenKey>> ProcessConfiguration(string configuration)
        {
            var keys = new BindableCollection<BindableCollection<OnScreenKey>>();

            if(string.IsNullOrWhiteSpace(configuration))
                return keys;

            var rows = configuration.Split(new[] {"]["}, StringSplitOptions.RemoveEmptyEntries);

            foreach(var row in rows)
            {
                var rowOfKeys = new BindableCollection<OnScreenKey>();
                var configuredKeys = row.Split(new[] {"^"}, StringSplitOptions.None);

                foreach(var configuredKey in configuredKeys)
                {
                    var versions = configuredKey.Split(new[] {"|"}, StringSplitOptions.RemoveEmptyEntries).ToList();
                    var versionText = versions.First();
                    versions.RemoveAt(0);

                    var alternateVersions = new List<OnScreenKeyConfiguration>();

                    foreach(var version in versions)
                    {
                        var keyConfig = DetermineKeyParts(version);

                        alternateVersions.Add(new OnScreenKeyConfiguration
                                              {
                                                  DisplayText = keyConfig.DisplayText,
                                                  Value = TryConvertToKey(keyConfig.Value),
                                                  TextValue = keyConfig.TextValue,
                                                  Style = TryConvertToStyle(keyConfig.Style)
                                              });
                    }

                    var configuredKeyParts = DetermineKeyParts(versionText);
                    var key = new OnScreenKey(configuredKeyParts.DisplayText, TryConvertToKey(configuredKeyParts.Value), configuredKeyParts.TextValue, TryConvertToStyle(configuredKeyParts.Style), alternateVersions);
                    rowOfKeys.Add(key);
                }

                keys.Add(rowOfKeys);
            }

            return keys;
        }

        private KeyConfig DetermineKeyParts(string versionText)
        {
            if(string.IsNullOrWhiteSpace(versionText))
                return null;

            var versionTextTokens = versionText.Split(new[] {":"}, StringSplitOptions.None);
            var keyConfig = new KeyConfig();

            foreach(var token in versionTextTokens)
            {
                if(token.StartsWith("tv=", StringComparison.InvariantCultureIgnoreCase))
                {
                    keyConfig.TextValue = token.Substring(3);
                    continue;
                }
                
                if(token.StartsWith("v=", StringComparison.InvariantCultureIgnoreCase))
                {
                    keyConfig.Value = token.Substring(2);
                    continue;
                }

                if(token.StartsWith("s=", StringComparison.InvariantCultureIgnoreCase))
                {
                    keyConfig.Style = token.Substring(2);
                    continue;
                }

                keyConfig.DisplayText = token;
            }

            return keyConfig;
        }

        private Key? TryConvertToKey(string value)
        {
            try
            {
                return Enum.TryParse(value, true, out Key parsedValue)
                    ? parsedValue
                    : (Key?)null;
            }
            catch(Exception)
            {
                return null;
            }
        }

        private Style TryConvertToStyle(string style)
        {
            if(string.IsNullOrWhiteSpace(style))
                return null;

            try
            {
                return Resources[style] as Style;
            }
            catch(Exception)
            {
                return null;
            }
        }

        private void OnScreenKeyClick(object sender, RoutedEventArgs routedEventArgs)
        {
            routedEventArgs.Handled = true;

            if(!(sender is OnScreenKey key))
                return;

            KeyTriggered?.Invoke(this, new KeyEventArgs
            {
                Key = key
            });
        }

        private int DetermineNumberOfVersions() => (Keys != null && Keys.Any()) ? Keys.SelectMany(row => row)?.Max(key => key.Versions) ?? 0 : 0;

        private class KeyConfig
        {
            public string DisplayText { get; set; }
            public string Value { get; set; }
            public string TextValue { get; set; }
            public string Style { get; set; }
        }
    }
}