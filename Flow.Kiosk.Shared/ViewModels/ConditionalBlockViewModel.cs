﻿//using System.Collections.Generic;
//using Flow.Core;
//
//namespace Flow.Kiosk.Shared.ViewModels
//{
//    public abstract class ConditionalBlockViewModel : BlockViewModel, IConditionalBlock
//    {
//        public ConditionalBlockViewModel(string name, string description = null)
//            : base(name, description)
//        {
//            ConnectionPoints = new HashSet<object>();
//        }
//
//        public abstract object Evaluate();
//
//        public HashSet<object> ConnectionPoints { get; }
//    }
//}