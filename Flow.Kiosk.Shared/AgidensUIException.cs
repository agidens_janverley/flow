﻿using System;

namespace Flow.Kiosk.Shared
{
    public class AgidensUIException : Exception
    {
        /// <summary>
        /// Exception with UI information
        /// </summary>
        /// <param name="number">The errornumber</param>
        /// <param name="message">An exception message</param>
        /// <param name="innerException">The actual exception</param>
        /// <param name="module">The module where the exception occured.</param>
        public AgidensUIException(int number, string message, Exception innerException, string module)
            : base(message, innerException)
        {
            Module = module;
            Timestamp = DateTime.Now;
            Number = number;
        }

        /// <summary>
        /// Exception with UI information
        /// </summary>
        /// <param name="number">The errornumber</param>
        /// <param name="message">An exception message</param>
        /// <param name="innerException">The actual exception</param>
        public AgidensUIException(int number, string message, Exception innerException)
            : this(number, message, innerException, "")
        {
        }

        /// <summary>
        /// Exception with UI information
        /// </summary>
        /// <param name="number">The errornumber</param>
        /// <param name="message">An exception message</param>
        public AgidensUIException(int number, string message) : this(number, message, null)
        {
        }

        /// <summary>
        /// The module the Exception occured in
        /// </summary>
        public string Module { get; set; }
        /// <summary>
        /// The exception number
        /// </summary>
        public int Number { get; set; }
        /// <summary>
        /// The timestamp of whem the exception occured
        /// </summary>
        public DateTime Timestamp { get; set; }
    }
}